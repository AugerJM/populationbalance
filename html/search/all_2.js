var searchData=
[
  ['dilution',['dilution',['../classdistribution.html#a1d45a7ad602674a118202f94e4c134db',1,'distribution::dilution()'],['../classdistributionquant.html#aa097aacbf9c08f079bb07693a142f3ff',1,'distributionquant::dilution()']]],
  ['dirichlet',['dirichlet',['../classdistribution.html#ac0219c457100bae6635735eb5854d133',1,'distribution']]],
  ['distance',['distance',['../classdistribution.html#afbe058c51b5698ef6f20a9c480692fce',1,'distribution::distance()'],['../classdistributionquant.html#a26be22fc4995a07a38b4c222d8064d6d',1,'distributionquant::distance()']]],
  ['distribution',['distribution',['../classdistribution.html',1,'distribution'],['../classdistribution.html#a91f2666c554d76395f0ae7df0a418c98',1,'distribution::distribution(void)'],['../classdistribution.html#ad39b724fee7b3d193929e4aa05b8cbdc',1,'distribution::distribution(std::string nom)'],['../classdistribution.html#a7bc76f5df05110d72cad333ef7700bcf',1,'distribution::distribution(const distribution &amp;dis)']]],
  ['distribution_2eh',['Distribution.h',['../_distribution_8h.html',1,'']]],
  ['distributionquant',['distributionquant',['../classdistributionquant.html',1,'distributionquant'],['../classdistributionquant.html#a78086641a803312524aef9099cdce996',1,'distributionquant::distributionquant(void)'],['../classdistributionquant.html#a63ef18f68bd6feaea2cbf3418516146f',1,'distributionquant::distributionquant(std::string nom)'],['../classdistributionquant.html#a8453cf5689b574dbd65d927730fe0904',1,'distributionquant::distributionquant(const distributionquant &amp;dis)']]],
  ['distributionquant_2eh',['DistributionQuant.h',['../_distribution_quant_8h.html',1,'']]]
];
