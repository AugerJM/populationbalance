var files =
[
    [ "Distribution.h", "_distribution_8h.html", [
      [ "distribution", "classdistribution.html", "classdistribution" ]
    ] ],
    [ "DistributionQuant.h", "_distribution_quant_8h.html", [
      [ "distributionquant", "classdistributionquant.html", "classdistributionquant" ]
    ] ],
    [ "Intro.h", "_intro_8h_source.html", null ],
    [ "Noyau.h", "_noyau_8h.html", [
      [ "noyau", "classnoyau.html", "classnoyau" ],
      [ "noyauConst", "classnoyau_const.html", "classnoyau_const" ],
      [ "noyauSmolBrown", "classnoyau_smol_brown.html", "classnoyau_smol_brown" ],
      [ "noyauSmolLam", "classnoyau_smol_lam.html", "classnoyau_smol_lam" ],
      [ "noyauCampStein", "classnoyau_camp_stein.html", "classnoyau_camp_stein" ],
      [ "noyauSaffTurner", "classnoyau_saff_turner.html", "classnoyau_saff_turner" ],
      [ "noyauLevich", "classnoyau_levich.html", "classnoyau_levich" ],
      [ "noyauDecant", "classnoyau_decant.html", "classnoyau_decant" ],
      [ "effPnueli", "classeff_pnueli.html", "classeff_pnueli" ],
      [ "effPnueliBrown", "classeff_pnueli_brown.html", "classeff_pnueli_brown" ],
      [ "noyauNormal", "classnoyau_normal.html", "classnoyau_normal" ],
      [ "noyauEquilibre", "classnoyau_equilibre.html", "classnoyau_equilibre" ],
      [ "noyauAggProd", "classnoyau_agg_prod.html", "classnoyau_agg_prod" ],
      [ "noyauAggSom", "classnoyau_agg_som.html", "classnoyau_agg_som" ],
      [ "noyauDislPuis", "classnoyau_disl_puis.html", "classnoyau_disl_puis" ],
      [ "noyauDislModeleExp1", "classnoyau_disl_modele_exp1.html", "classnoyau_disl_modele_exp1" ],
      [ "noyauDislModeleExp2", "classnoyau_disl_modele_exp2.html", "classnoyau_disl_modele_exp2" ],
      [ "noyauAggModeleBrown", "classnoyau_agg_modele_brown.html", "classnoyau_agg_modele_brown" ],
      [ "noyauAggModeleTurb1", "classnoyau_agg_modele_turb1.html", "classnoyau_agg_modele_turb1" ],
      [ "noyauAggModeleTurb2", "classnoyau_agg_modele_turb2.html", "classnoyau_agg_modele_turb2" ]
    ] ]
];