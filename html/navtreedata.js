var NAVTREE =
[
  [ "PopulationBalance C++ framework", "index.html", [
    [ "Population balance", "index.html", [
      [ "General informations about models", "index.html#intro_sec", null ],
      [ "General structure of C++ classes", "index.html#struc_sec", null ],
      [ "Limitations", "index.html#limit_sec", null ],
      [ "Installation", "index.html#install_sec", [
        [ "Needed tools", "index.html#tools_subsec", null ],
        [ "Running", "index.html#running", null ]
      ] ],
      [ "Copyright and License", "index.html#copyright", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_distribution_8h.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';