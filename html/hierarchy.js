var hierarchy =
[
    [ "distribution", "classdistribution.html", null ],
    [ "distributionquant", "classdistributionquant.html", null ],
    [ "noyau", "classnoyau.html", [
      [ "effPnueli", "classeff_pnueli.html", null ],
      [ "effPnueliBrown", "classeff_pnueli_brown.html", null ],
      [ "noyauAggModeleBrown", "classnoyau_agg_modele_brown.html", null ],
      [ "noyauAggModeleTurb1", "classnoyau_agg_modele_turb1.html", null ],
      [ "noyauAggModeleTurb2", "classnoyau_agg_modele_turb2.html", null ],
      [ "noyauAggProd", "classnoyau_agg_prod.html", null ],
      [ "noyauAggSom", "classnoyau_agg_som.html", null ],
      [ "noyauCampStein", "classnoyau_camp_stein.html", null ],
      [ "noyauConst", "classnoyau_const.html", null ],
      [ "noyauDecant", "classnoyau_decant.html", null ],
      [ "noyauDislModeleExp1", "classnoyau_disl_modele_exp1.html", null ],
      [ "noyauDislModeleExp2", "classnoyau_disl_modele_exp2.html", null ],
      [ "noyauDislPuis", "classnoyau_disl_puis.html", null ],
      [ "noyauEquilibre", "classnoyau_equilibre.html", null ],
      [ "noyauLevich", "classnoyau_levich.html", null ],
      [ "noyauNormal", "classnoyau_normal.html", null ],
      [ "noyauSaffTurner", "classnoyau_saff_turner.html", null ],
      [ "noyauSmolBrown", "classnoyau_smol_brown.html", null ],
      [ "noyauSmolLam", "classnoyau_smol_lam.html", null ]
    ] ]
];