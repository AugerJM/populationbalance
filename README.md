# PopulationBalance

PopulationBalance is a C++ library for population balance problems simulation applied to clusters aggregation and breakage in liquids.
It was constructed around the following restrictions :

- Quantization of clusters sizes
- Concentrations continuity
- Closed system
- Reversible binary aggregations
- First order kinetics (e.g. mass action kinetics)

## Compilation/Installation

Method 1 : systems using .deb packages (Ubuntu, Debian...)

On systems using .deb packages (Ubuntu, Debian...), a binary package containing
the library, command-line utilities, and the corresponding documentation can be
built using "dpkg-buildpackage -us -uc" command in the sources directory. 
Examples in the examples folder are compiled at the same time.The 
resulting .deb (populationbalance_N°_Arch.deb) file can then be installed and
uninstalled using standard packages managers (for example by the command 
"sudo dpkg -i populationbalance_N°_Arch.deb" for installation). 

Method 2 : Linux, Mac OS, and Windows (using Cygwin)

The package contains the C++ source code, example files, miscellaneous utilities
and documentation. On Linux, Mac OS, and Windows (using Cygwin), the
compilation and installation can be carried out using GNU Make.

The "make" command in the sources direcory will compile the library, command-line
utilities, and examples. The command-line utility and library will appear within 
the src sub-directory, while examples will be in the examples sub-directory.

Following successful compilation, the library, command-line utility, and
documentation can be installed by typing "sudo make install". Framework files are 
installed into /usr/local.

The code can later be uninstalled with "sudo make uninstall".


Note : it may be necessary to modify your environment variables in order to 
access the installed files

- to use the command-line utilities, the variable PATH should contain
  /usr/local/bin.
- to access the man pages, the variable MANPATH should contain
  /usr/local/man.

## Use of the library

To link to the library, code compilation should include the
flags '-L/usr/local/lib' to tell the linker where to look, and then
'-lBilanPop' to link to the library.

## Contents

examples - documented examples making use of the library

html - Doxygen generated HTML reference manual (starting page index.html)

man - man pages installed with the program

src - source code files