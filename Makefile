prefix = /usr/local
CC = g++
CFLAGS = -O2 -s -Wall -std=c++11 -fPIC

all : src/libBilanPop.so src/PopBalEq src/KernelGen src/KernelSum src/KernelProd src/ExpFit src/ExpFitStat examples/BasicSimulation/ExampleSimplePopulationBalance examples/BoundaryConditions/ExampleNeumann examples/ContinuousVsDiscrete/ExampleDiscCont examples/Distances/ExampleDistances examples/InitialStates/ExampleInitialStates

src/libBilanPop.so : src/Distribution.o src/Noyau.o src/DistributionQuant.o
	$(CC) -o src/libBilanPop.so -shared src/Distribution.o src/Noyau.o src/DistributionQuant.o

src/PopBalEq : src/PopBalEq.o
	$(CC) -Lsrc src/PopBalEq.cpp -o src/PopBalEq -lBilanPop

src/KernelGen : src/KernelGen.o
	$(CC) -Lsrc src/KernelGen.cpp -o src/KernelGen -lBilanPop

src/KernelSum : src/KernelSum.o
	$(CC) -Lsrc src/KernelSum.cpp -o src/KernelSum -lBilanPop

src/KernelProd : src/KernelProd.o
	$(CC) -Lsrc src/KernelProd.cpp -o src/KernelProd -lBilanPop

src/ExpFit : src/ExpFit.o
	$(CC) -Lsrc src/ExpFit.cpp -o src/ExpFit -lBilanPop

src/ExpFitStat : src/ExpFitStat.o
	$(CC) -Lsrc src/ExpFitStat.cpp -o src/ExpFitStat -lBilanPop

examples/BasicSimulation/ExampleSimplePopulationBalance : examples/BasicSimulation/ExampleSimplePopulationBalance.o
	$(CC) -Isrc -Lsrc examples/BasicSimulation/ExampleSimplePopulationBalance.cpp -o examples/BasicSimulation/ExampleSimplePopulationBalance -lBilanPop

examples/BoundaryConditions/ExampleNeumann : examples/BoundaryConditions/ExampleNeumann.o
	$(CC) -Isrc -Lsrc examples/BoundaryConditions/ExampleNeumann.cpp -o examples/BoundaryConditions/ExampleNeumann -lBilanPop

examples/ContinuousVsDiscrete/ExampleDiscCont : examples/ContinuousVsDiscrete/ExampleDiscCont.o
	$(CC) -Isrc -Lsrc examples/ContinuousVsDiscrete/ExampleDiscCont.cpp -o examples/ContinuousVsDiscrete/ExampleDiscCont -lBilanPop

examples/Distances/ExampleDistances : examples/Distances/ExampleDistances.o
	$(CC) -Isrc -Lsrc examples/Distances/ExampleDistances.cpp -o examples/Distances/ExampleDistances -lBilanPop

examples/InitialStates/ExampleInitialStates : examples/InitialStates/ExampleInitialStates.o
	$(CC) -Isrc -Lsrc examples/InitialStates/ExampleInitialStates.cpp -o examples/InitialStates/ExampleInitialStates -lBilanPop

%.o: %.cpp
	$(CC) $(CFLAGS) -Isrc -o $@ -c $<

install:
	gzip < man/KernelGen.1 > man/KernelGen.1.gz
	gzip < man/KernelProd.1 > man/KernelProd.1.gz
	gzip < man/KernelSum.1 > man/KernelSum.1.gz
	gzip < man/PopBalEq.1 > man/PopBalEq.1.gz
	gzip < man/ExpFit.1 > man/ExpFit.1.gz
	gzip < man/ExpFitStat.1 > man/ExpFitStat.1.gz
	install -m 0755 -D src/ExpFit \
                $(DESTDIR)$(prefix)/bin/ExpFit
	install -m 0755 -D src/ExpFitStat \
                $(DESTDIR)$(prefix)/bin/ExpFitStat
	install -m 0755 -D src/KernelGen \
                $(DESTDIR)$(prefix)/bin/KernelGen
	install -m 0755 -D src/KernelProd \
                $(DESTDIR)$(prefix)/bin/KernelProd
	install -m 0755 -D src/KernelSum \
                $(DESTDIR)$(prefix)/bin/KernelSum
	install -m 0755 -D src/PopBalEq \
                $(DESTDIR)$(prefix)/bin/PopBalEq
	install -m 644 -D man/KernelGen.1.gz \
                $(DESTDIR)$(prefix)/man/man1/KernelGen.1.gz
	install -m 644 -D man/ExpFitStat.1.gz \
                $(DESTDIR)$(prefix)/man/man1/ExpFitStat.1.gz
	install -m 644 -D man/ExpFit.1.gz \
                $(DESTDIR)$(prefix)/man/man1/ExpFit.1.gz
	install -m 644 -D man/KernelProd.1.gz \
                $(DESTDIR)$(prefix)/man/man1/KernelProd.1.gz
	install -m 644 -D man/KernelSum.1.gz \
                $(DESTDIR)$(prefix)/man/man1/KernelSum.1.gz
	install -m 644 -D man/PopBalEq.1.gz \
                $(DESTDIR)$(prefix)/man/man1/PopBalEq.1.gz
	install -D src/libBilanPop.so \
                $(DESTDIR)$(prefix)/lib/libBilanPop.so
	install -D src/Distribution.h \
                $(DESTDIR)$(prefix)/include/Distribution.h
	install -D src/DistributionQuant.h \
                $(DESTDIR)$(prefix)/include/DistributionQuant.h
	install -D src/Noyau.h \
                $(DESTDIR)$(prefix)/include/Noyau.h
	-rm -f man/PopBalEq.1.gz
	-rm -f man/KernelSum.1.gz
	-rm -f man/KernelProd.1.gz
	-rm -f man/ExpFit.1.gz
	-rm -f man/ExpFitStat.1.gz
	-rm -f man/KernelGen.1.gz

clean:
	-rm -f src/ExpFit
	-rm -f src/ExpFitStat
	-rm -f src/KernelGen
	-rm -f src/KernelProd
	-rm -f src/KernelSum
	-rm -f src/PopBalEq
	-rm -f src/libBilanPop.so
	-rm -f src/PopBalEq.o
	-rm -f src/KernelGen.o
	-rm -f src/KernelSum.o
	-rm -f src/KernelProd.o
	-rm -f src/ExpFit.o
	-rm -f src/ExpFitStat.o
	-rm -f src/Distribution.o
	-rm -f src/Noyau.o
	-rm -f src/DistributionQuant.o
	-rm -f examples/BasicSimulation/ExampleSimplePopulationBalance.o
	-rm -f examples/BoundaryConditions/ExampleNeumann.o
	-rm -f examples/ContinuousVsDiscrete/ExampleDiscCont.o
	-rm -f examples/Distances/ExampleDistances.o
	-rm -f examples/InitialStates/ExampleInitialStates.o

distclean: clean

help: src/Doxyfile
	doxygen src/Doxyfile

uninstall:
	-rm -f $(DESTDIR)$(prefix)/bin/ExpFit
	-rm -f $(DESTDIR)$(prefix)/bin/ExpFitStat
	-rm -f $(DESTDIR)$(prefix)/bin/KernelGen
	-rm -f $(DESTDIR)$(prefix)/bin/KernelProd
	-rm -f $(DESTDIR)$(prefix)/bin/KernelSum
	-rm -f $(DESTDIR)$(prefix)/bin/PopBalEq
	-rm -f $(DESTDIR)$(prefix)/man/man1/KernelGen.1.gz
	-rm -f $(DESTDIR)$(prefix)/man/man1/ExpFitStat.1.gz
	-rm -f $(DESTDIR)$(prefix)/man/man1/ExpFit.1.gz
	-rm -f $(DESTDIR)$(prefix)/man/man1/KernelProd.1.gz
	-rm -f $(DESTDIR)$(prefix)/man/man1/KernelSum.1.gz
	-rm -f $(DESTDIR)$(prefix)/man/man1/PopBalEq.1.gz
	-rm -f $(DESTDIR)$(prefix)/lib/libBilanPop.so
	-rm -f $(DESTDIR)$(prefix)/include/DistributionQuant.h
	-rm -f $(DESTDIR)$(prefix)/include/Distribution.h
	-rm -f $(DESTDIR)$(prefix)/include/Noyau.h
	-rm -f examples/BasicSimulation/ExampleSimplePopulationBalance
	-rm -f examples/BoundaryConditions/ExampleNeumann
	-rm -f examples/ContinuousVsDiscrete/ExampleDiscCont
	-rm -f examples/Distances/ExampleDistances
	-rm -f examples/InitialStates/ExampleInitialStates
