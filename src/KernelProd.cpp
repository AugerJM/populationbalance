//Générateur de noyaux pour bilan de populations : somme de 2 noyaux fournis en fichiers texte

#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include "Noyau.h"
#include "Distribution.h"

using namespace std;



int main(int argc, char* argv[])
{
	// Argument 1 : noyau1 (fichier)
	// Argument 2 : noyau2 (fichier)
	// Argument 3 : noyau somme (fichier)

	// Vérification nombre + conversions types arguments
	if (argc!=4)
	{
		cout << "Mauvais nombre d'arguments" << endl;
		return 1;
	}

	noyau kernel(argv[1],1);
	noyau kernel1(argv[2],1);
	kernel.produit(&kernel1);
	kernel.ecrire(argv[3],1);

    	return 0;
 }