///\~french @file      	Noyau.h
///\~french @author    	JMA
///\~french @version   	1.0
///\~french @date      	30/06/2017
///\~french @brief     	Définit un noyau d'aggrégation/dislocation
///\~french @details   	Ces classes définissent les divers types de noyaux existants

///\~english @file      	Noyau.h
///\~english @author    	JMA
///\~english @version   	1.0
///\~english @date      	30/06/2017
///\~english @brief     	Defines an aggregation/breakage kernel
///\~english @details   	These classes define various existing kernel types				 

#ifndef NOYAU_H
#define NOYAU_H

#include <string>
#include <vector>
#include "Distribution.h"

class distribution;
	
/// \~french @brief      	Objet : noyau d'aggrégation/dislocation
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation et dislocation, ainsi que les opérations standard. Version générale ne correspondant à aucun modèle de noyau en particuler.

/// \~english @brief      	Object : aggregation/breakage kernel
/// \~english @details    	Contains kinetic coefficients for aggregation and breakage, and standard kernel operations. General version with no particular kernel model.
class noyau
{
	protected:
	
	/// \~french @brief 		Nombre de classes définies dans le noyau (en partant de la classe de particules unitaires).
	/// \~french @details    	Les coefficients correspondant à des classes supérieures sont considérées nuls (classes non réactives).

	/// \~english @brief 		Kernel size (starting from unitary clusters).
	/// \~english @details    	Kinetics coefficients for indices greater than the size are considered equal to zero (non reactive).
	int nbclasses;


	/// \~french @brief 		Eléments du noyau d'aggrégation
	/// \~english @brief 		Coefficients of aggregation kernel
	std::vector<double> noyauaggr;


	/// \~french @brief 		Eléments du noyau de fragmentation
	/// \~english @brief 		Coefficients of breakage kernel
	std::vector<double> noyaudisl;
 
	public:
	
	/// \~french @brief      	Constructeur de base
	/// \~english @brief      	Base constructor
	noyau (void);


	/// \~french @brief      	Constructeur à partir d'un fichier unique
	/// \~french @param    		nom			Nom du ficher contenant le noyau				
	/// \~french @param    		categorie		Type de noyau considéré dans le fichier (1-aggrégation/2-dislocation)

	/// \~english @brief      	Constructor from unique file
	/// \~english @param    	nom			Filename containing kernel coefficients				
	/// \~english @param    	categorie		Kernel type indicator (1-aggregation/2-breakage)
	noyau (std::string nom, int categorie);


	/// \~french @brief      	Constructeur à partir de 2 fichiers
	/// \~french @param    		nom			Nom du ficher contenant le noyau d'aggrégation		
	/// \~french @param    		nom2			Nom du ficher contenant le noyau de dislocation

	/// \~english @brief      	Constructor from 2 files
	/// \~english @param    	nom			Filename containing aggregation kernel coefficients			
	/// \~english @param    	nom2			Filename containing breakage kernel coefficients	
	noyau (std::string nom, std::string nom2);

	
	/// \~french @brief      	Ecriture des éléments du noyau à l'écran
	/// \~french @details    	La première liste affichée correspond aux coefficients d'aggrégation
	///			    	et la seconde aux coefficients de dislocation.

	/// \~english @brief      	Writes kernel coefficients on screen
	/// \~english @details    	The first list corresponds to aggregation coefficients
	///			    	the second one to breakage coefficients.
	void ecrire (void) const;


	/// \~french @brief      	Ecriture des éléments du noyau dans un fichier
	/// \~french @details    	Ecriture en pyramide des coefficients; écrase les données déjà présentes dans le fichier 
	/// \~french @param    		nom			Nom du fichier 
	/// \~french @param    		categorie		Type des coefficients 1-aggrégation/2-dislocation

	/// \~english @brief      	Writes kernel coefficients in file
	/// \~english @details    	Coefficients are ordered in pyramid; existing data on the file are removed 
	/// \~english @param    	nom			Filename 
	/// \~english @param    	categorie		Kernel type 1-aggregation/2-breakage 
	void ecrire (std::string nom, int categorie) const;
	

	/// \~french @brief     	Renvoie les éléments du noyau
	/// \~french @details    	Pour les coefficients d'aggrégation, renvoie la vitesse d'aggrégation entre classes i et j.
	///			    	Pour les coefficients de dislocation, renvoie la vitesse de dislocation des particules de classe i+j en particules i et j.
	/// \~french @param    		i			Entier >0
	/// \~french @param    		j			Entier >0
	/// \~french @param    		categorie		Type du coefficient 1-aggrégation/2-dislocation 
	/// \~french @return    	La valeur du coefficient

	/// \~english @brief     	Returns kernel coefficient
	/// \~english @details    	For aggregation kernel, returns aggregation kinetic coefficient between clusters of sizes i and j.
	///			    	For breakage kernel, returns kinetic coefficient for clusters of size i+j breaking in clusters sizes i and j.
	/// \~english @param    	i			Positive integer
	/// \~english @param    	j			Positive integer
	/// \~english @param    	categorie		Kernel type 1-aggregation/2-breakage 
	/// \~english @return    	Kinetic coefficient value
	double element(int i, int j, unsigned int categorie) const;


	/// \~french @brief      	Renvoie le nombre de classes du noyau
	/// \~french @details    	Utilisation interne (principalement pour la fonction d'addition)
	/// \~french @return    	Le nombre de classes explicitées du noyau

	/// \~english @brief     	Returns kernel size
	/// \~english @details    	Internal function (neede for kernels summations)
	/// \~english @return    	Kernel size (integer)
	int taille(void) const;


	/// \~french @brief      	Ajout d'un autre noyau
	/// \~french @details    	Somme les différents éléments (aggrégation et dislocation) de deux noyaux.
	///			    	Note : le noyau est redimensionné au nombre de classes du plus grand des deux noyaux ajoutés.
	/// \~french @param    		noy2			Pointeur vers le noyau à ajouter

	/// \~english @brief      	Adds another kernel to the current one
	/// \~english @details    	Corresponding coefficients from the 2 added kernels are summed.
	///			    	Note : the current kernel is resized to the biggest from the 2 summed kernels.
	/// \~english @param    	noy2			Pointer to kernel to be added	
	void ajout(noyau * noy2);


	/// \~french @brief      	Multiplication par un autre noyau
	/// \~french @details    	Produit terme à terme des différents éléments (aggrégation et dislocation) de deux noyaux.
	///			    	Note : le noyau est redimensionné au nombre de classes du plus grand des deux noyaux multipliés.
	///			    	Note 2 : l'utilisation principale est la multiplication des noyaux standards par un coefficient d'efficacité.
	/// \~french @param    		noy2			Pointeur vers le noyau à multiplier	

	/// \~english @brief      	Product with another kernel
	/// \~english @details    	Corresponding coefficients from the 2 added kernels are multiplied.
	///			    	Note : the current kernel is resized to the biggest from the 2 multiplied kernels.
	///			    	Note 2 : main use is to apply an efficiency coefficient to a given kernel
	/// \~english @param    	noy2			Pointer to the kernel to be multiplied with	
	void produit(noyau * noy2);


	/// \~french @brief      	Passage à la puissance du noyau
	/// \~french @details    	Chaque terme du noyau est élevé à la puissance indiquée
	///			    	Note : l'utilisation principale est la génération de noyaux de type analytique.
	/// \~french @param    		expo			Exposant

	/// \~english @brief      	Elevates kernel coefficients to a given exponent
	/// \~english @details    	Each kernel coefficient is elevated to the same exponent
	///			    	Note : Main use is to generate analytic kernels.
	/// \~english @param    	expo			Exponent
	void puissance(double expo);


	/// \~french @brief      	Echange entre noyau d'agrégation et de fragmentation
	/// \~french @details    	Echange terme à terme entre les noyaux d'agrégation et de fragmentation
	///			    	Note : évite la redondance des générateurs de noyaux.

	/// \~english @brief      	Exchanges coefficients for aggregation and breakage for the current kernel
	/// \~english @details    	Corresponding aggregation and breakage coefficients are exchanged
	///			    	Note : used to avoid useless multiplication of kernel generating functions.	
	void echange(void);
};


/// \~french @brief      	Objet : Noyau d'aggrégation/dislocation constant
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation et dislocation, ainsi que les opérations standard. Version du noyau aggrégation/dislocation constant. (modèle mathématique abstrait \f$ k_{i,j} = aggr, {k_{i,j}}^* = disl \f$ ).

/// \~english @brief      	Object : Constant aggregation/breakage kernel
/// \~english @details    	Contains kinetic coefficients for aggregation and breakage, and standard kernel operations. Version for constant aggregation/breakage kernel. (abstract theoretical model \f$ k_{i,j} = aggr, {k_{i,j}}^* = disl \f$ ).
class noyauConst : public noyau
{
	public :
	/// \~french @brief      	Constructeur à partir de valeurs de coefficients
	/// \~french @param    		aggr			Coefficient constant d'aggrégation				
	/// \~french @param    		disl			Coefficient constant de dislocation
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	aggr			Constant aggregation coefficient value			
	/// \~english @param    	disl			Constant breakage coefficient value
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauConst (double aggr, double disl, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure Smoluchowski brownien
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation, ainsi que les opérations standard. Version du noyau aggrégation de type brownien de Smoluchowski. Hypothèse des aggrégats sphériques parfaitement denses (modèle physique \f$ k_{i,j} = {\frac {2.k.T}{3.mu}}.(R_i+R_j).(\frac {1}{R_i}+\frac {1}{R_j}), {k_{i,j}}^* = 0 \f$ ).

/// \~english @brief      	Object : Brownian Smoluchowski aggregation kernel
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for brownian Smoluchowski aggregation kernel. Perfectly packed spherical clusters hypothesis (abstract theoretical model \f$ k_{i,j} = {\frac {2.k.T}{3.mu}}.(R_i+R_j).(\frac {1}{R_i}+\frac {1}{R_j}), {k_{i,j}}^* = 0 \f$ ).
class noyauSmolBrown : public noyau
{
	public:
	/// \~french @brief     	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)				
	/// \~french @param    		T			Température (K) 
	/// \~french @param    		mu			Viscosté dynamique (Pa.s)
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief     	Constructor from model and numerical parameters
	/// \~english @param    	r			Unitary cluster radius (m)				
	/// \~english @param    	T			Temperature (K) 
	/// \~english @param    	mu			Dynamic viscosity (Pa.s)
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauSmolBrown (double r, double T, double mu, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure Smoluchowski laminaire
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation, ainsi que les opérations standard. Version du noyau aggrégation de type laminaire de Smoluchowski. Hypothèse des aggrégats sphériques parfaitement denses (modèle physique \f$ k_{i,j} = {\frac {4}{3}}.cis.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ ).

/// \~english @brief      	Object : Laminar Smoluchowski aggregation kernel
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for laminar Smoluchowski aggregation kernel. Perfectly packed spherical clusters hypothesis (abstract theoretical model \f$ k_{i,j} = {\frac {4}{3}}.cis.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ ).
class noyauSmolLam : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)				
	/// \~french @param    		cis			Vitesse de cisaillement (\f$ s^{-1} \f$) 
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	r			Unitary cluster radius (m)				
	/// \~english @param    	cis			Shear rate (\f$ s^{-1} \f$) 
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauSmolLam (double r, double cis, int taille);
};


/// \~french @brief      	Objet noyau d'aggregation pure Camp et Stein
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation, ainsi que les opérations standard. Version du noyau d'aggrégation de type turbulent, basé sur une homogénéisétion du cas de l'écoulement laminaire. Hypothèse des aggrégats sphériques parfaitement denses. Hypothèse d'identité des vitesse particule/fluide (modèle physique \f$ k_{i,j} = {\frac {4}{3}}.(\frac {epsilon}{nu})^{1/2}.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ ).

/// \~english @brief      	Object : Camp and Stein aggregation kernel
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for turbulent aggregation kernel, based on homogenised laminar flow. Perfectly packed spherical clusters and  perfectly dragged clusters hypotheses (abstract theoretical model \f$ k_{i,j} = {\frac {4}{3}}.(\frac {epsilon}{nu})^{1/2}.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ ).
class noyauCampStein : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)				
	/// \~french @param    		nu			Viscosité cinématique (\f$ m^2.s^{-1} \f$)
	/// \~french @param    		epsilon			Vitesse de dissipation d'énergie fluide massique (\f$ J.kg^{-1}.s^{-1}\f$)	
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	r			Unitary cluster radius (m)				
	/// \~english @param    	nu			Kinematic viscosity (\f$ m^2.s^{-1} \f$)
	/// \~english @param    	epsilon			Fluid mass dissipation rate (\f$ J.kg^{-1}.s^{-1}\f$)	
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauCampStein (double r, double nu, double epsilon, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure Saffman et Turner 
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation, ainsi que les opérations standard. Version du noyau d'aggrégation de type turbulent, basé sur une homogénéisétion du cas de l'écoulement laminaire. Hypothèse des aggrégats sphériques parfaitement denses. Prise en compte de la vitesse relative entre fluide et particules (modèle physique \f$ k_{i,j} = \sqrt {\frac {8.\pi}{15}}.(\frac {epsilon}{nu})^{1/2}.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ ).

/// \~english @brief      	Object : Saffman and Turner aggregation kernel
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for turbulent aggregation kernel, based on homogenised laminar flow. Perfectly packed spherical clusters hypothesis. Relative displacement between fluid and clusters taken into account (abstract theoretical model \f$ k_{i,j} = \sqrt {\frac {8.\pi}{15}}.(\frac {epsilon}{nu})^{1/2}.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ ).
class noyauSaffTurner : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)				
	/// \~french @param    		nu			Viscosité cinématique (\f$ m^2.s^{-1} \f$)
	/// \~french @param    		epsilon			Vitesse de dissipation d'énergie fluide massique (\f$ J.kg^{-1}.s^{-1}\f$)	
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructeur à partir de valeurs physiques
	/// \~english @param    	r			Unitary cluster radius (m)				
	/// \~english @param    	nu			Kinematic viscosity (\f$ m^2.s^{-1} \f$)
	/// \~english @param    	epsilon			Fluid mass dissipation rate (\f$ J.kg^{-1}.s^{-1}\f$)	
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauSaffTurner (double r, double nu, double epsilon, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure Levich 
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation, ainsi que les opérations standard. Version du noyau d'aggrégation de type turbulent, basé sur une homogénéisétion du cas de l'écoulement laminaire. Hypothèse des aggrégats sphériques parfaitement denses. Prise en compte de la vitesse relative entre fluide et particules? (modèle physique \f$ k_{i,j} = 13,8.(\frac {epsilon}{nu})^{1/2}.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ )

/// \~english @brief      	Object : Levich aggregation kernel
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for turbulent aggregation kernel, based on homogenised laminar flow. Perfectly packed spherical clusters hypothesis. Relative displacement between fluid and clusters taken into account ? (abstract theoretical model \f$ k_{i,j} = 13,8.(\frac {epsilon}{nu})^{1/2}.(R_i+R_j)^3, {k_{i,j}}^* = 0 \f$ )
class noyauLevich : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)				
	/// \~french @param    		nu			Viscosité cinématique (\f$ m^2.s^{-1} \f$)
	/// \~french @param    		epsilon			Vitesse de dissipation d'énergie fluide massique (\f$ J.kg^{-1}.s^{-1}\f$)	
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	r			Unitary cluster radius (m)			
	/// \~english @param    	nu			Kinematic viscosity (\f$ m^2.s^{-1} \f$)
	/// \~english @param    	epsilon			Fluid mass dissipation rate (\f$ J.kg^{-1}.s^{-1}\f$)	
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauLevich (double r, double nu, double epsilon, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure par décantation
/// \~french @details    	Contient les coefficients des noyaux d'aggrégation, ainsi que les opérations standard. Version du noyau d'aggrégation de type décantation. Hypothèse des aggrégats sphériques parfaitement denses. Hypothèse du fluide quasi-immobile et d'une vitesse stabilisée.

/// \~english @brief      	Object : Settling aggregation kernel
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for turbulent aggregation kernel. Perfectly packed spherical clusters hypothesis. Unmoving fluid and stabilised particle speed hypotheses.
class noyauDecant : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)				
	/// \~french @param    		mu			Viscosté dynamique (Pa.s)
	/// \~french @param    		rho			Masse volumique des particules (\f$ kg.m^{-3} \f$)	
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	r			Unitary cluster radius (m)				
	/// \~english @param    	mu			Kinematic viscosity (Pa.s)
	/// \~english @param    	rho			Clusters density (\f$ kg.m^{-3} \f$)	
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauDecant (double r, double mu, double rho, int taille);
};


/// \~french @brief      	Objet : Noyau d'efficacité Pnueli (correction simple Saffman-Turner)
/// \~french @details    	Contient les coefficients des noyaux d'efficacité, ainsi que les opérations standard. Version du noyau d'efficacité de type Pnueli (correction simple écoulement de Saffman-Turner). Hypothèse des aggrégats sphériques parfaitement denses. Hypothèse d'un écoulement autour d'une particule -> asymétrie de l'efficacité.

/// \~english @brief      	Object : Pnueli efficiency kernel (simple correction for Saffman-Turner kernel)
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for Pnueli efficiency kernel (simple correction for Saffman-Turner kernel). Perfectly packed spherical clusters hypothesis. Hypothesis of fluid flow around clusters -> asymmetry of efficiency.
class effPnueli : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)				
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	r			Unitary cluster radius (m)				
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	effPnueli (double r, int taille);
};


/// \~french @brief      	Objet : Noyau d'efficacité Pnueli brownien (correction Saffman-Turner + agitation brownienne)
/// \~french @details    	Contient les coefficients des noyaux d'efficacité, ainsi que les opérations standard. Version du noyau d'efficacité de type Pnueli (correction simple écoulement de Saffman-Turner + effet brownien). Hypothèse des aggrégats sphériques parfaitement denses. Hypothèse d'un écoulement autour d'une particule -> asymétrie de l'efficacité.

/// \~english @brief      	Object : Brownian Pnueli efficiency kernel (simple correction for Saffman-Turner kernel + brownian movements)
/// \~english @details    	Contains kinetic coefficients for aggregation, and standard kernel operations. Version for Brownian Pnueli efficiency kernel (simple correction for Saffman-Turner kernel + brownian movements). Perfectly packed spherical clusters hypothesis. Hypothesis of fluid flow around clusters -> asymmetry of efficiency.
class effPnueliBrown : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de valeurs physiques
	/// \~french @param    		r			Rayon de la particule unitaire (m)
	/// \~french @param    		peclet			Nombre de Peclet (sans dimension)	
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	r			Unitary cluster radius (m)
	/// \~english @param    	peclet			Peclet number (dimensionless)	
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	effPnueliBrown (double r, double peclet, int taille);
};


/// \~french @brief      	Objet : Noyau aléatoire de distribution normale
/// \~french @details    	Constructeur de noyau aléatoire de distribution normale (utilisation principale : perturbation d'un noyau donné par ajout)

/// \~english @brief      	Object : Normally distributed random kernel
/// \~english @details    	Constructor for normally distributed random kernel (main use : disturbing an existing kernel by addition)
class noyauNormal : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir d'une distribution normale
	/// \~french @param    		ecarttype		Ecart-type de la loi normale utilisée
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)
	/// \~french @param    		type			Type du noyau généré 1-aggrégation/2-dislocation 

	/// \~english @brief      	Constructor for normally distributed random kernel
	/// \~english @param    	ecarttype		Standard deviation for normal distribution
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	/// \~english @param    	type			Generated kernel type (1-aggregation/2-breakage) 
	noyauNormal (double ecarttype, int taille, int type);


	/// \~french @brief      	Régénère le noyau de distribution normale (et permet également de créer une perturbation différente sur les noyaux de dislocation et aggrégation sur un même objet noyau)
	/// \~french @param    		ecarttype		Ecart-type de la loi normale utilisée
	/// \~french @param    		type			Type du noyau généré 1-aggrégation/2-dislocation

	/// \~english @brief      	Recomputes the normal distribution kernel (allows to apply a different normal distribution for aggregation and breakage parts of the same kernel)
	/// \~english @param    	ecarttype		Standard deviation for normal distribution
	/// \~english @param    	type			Generated kernel type (1-aggregation/2-breakage) 
	void reinitialise (double ecarttype, int type);


	/// \~french @brief      	Copie un noyau, et ajoute une perturbation aléatoire (distribuée selon loi normale) proportionnelle à ses coefficients 
	/// \~french @param    		ecarttype		Ecart-type de la loi normale utilisée (en % des coefficients du noyau de départ)
	/// \~french @param    		kernel			Noyau dont les coefficients doivent être perturbés de ecarttype %

	/// \~english @brief      	Copies a given kernel, and adds a random normal perturbation proportional to its coefficients 
	/// \~english @param    	ecarttype		Standard deviation for normal perturbation (% of kernel coefficients values)
	/// \~english @param    	kernel			Kernel to be copied and perturbed
	void perturbe (double ecarttype, noyau * kernel);
};


/// \~french @brief      	Objet : Noyau expérimental construit à partir de l'état d'équilibre donné par une distribution
/// \~french @details   	Constructeur de noyaux expérimentaux (hypothèse de l'équilibre local ou équilibre des complexes). Les composantes du noyau de fragmentation sont supposées égales à 1, l'ajustement se faisant sur le noyau d'aggrégation.

/// \~english @brief      	Object : Experimental kernel built from a steady state distribution
/// \~english @details   	Constructor for experimental kernel (complex equilibrium hypothesis). Breakage coefficients of the kernel are normalized to 1.
class noyauEquilibre : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir d'une distribution à l'équilibre
	/// \~french @param    		distr			Distribution à l'état d'équilibre

	/// \~english @brief      	Constructor from steady state distribution
	/// \~english @param    	distr			Steady state distribution used as basis
	noyauEquilibre (distribution * distr);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure de type produit
/// \~french @details    	Constructeur de noyaux analytiques proportionnels au produit du nombre de particules élémentaires des classes concernées (modèle mathématique abstrait \f$ k_{i,j} = k \cdot i \cdot j,{k_{i,j}}^* = 0 \f$)

/// \~english @brief      	Object : Product aggregation kernel
/// \~english @details    	Constructor for analytic kernel, with kinetic coefficients proportional to the product of aggregated clusters sizes (as a number of unitary clusters) (abstract theoretical model \f$ k_{i,j} = k \cdot i \cdot j,{k_{i,j}}^* = 0 \f$)
class noyauAggProd : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	k			Multiplicative factor
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauAggProd (double k, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure de type somme
/// \~french @details    	Constructeur de noyaux analytiques proportionnels à la somme du nombre de particules élémentaires des classes concernées (modèle mathématique abstrait \f$ k_{i,j} = k.(i+j),{k_{i,j}}^* = 0 \f$ )

/// \~english @brief      	Object : Sum aggregation kernel
/// \~english @details    	Constructor for analytic kernel, with kinetic coefficients proportional to the sum of aggregated clusters sizes (as a number of unitary clusters) (abstract theoretical model \f$ k_{i,j} = k.(i+j),{k_{i,j}}^* = 0 \f$ )
class noyauAggSom : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	k			Multiplicative factor
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauAggSom (double k, int taille);
};


/// \~french @brief      	Objet : Noyau de fragmentation pure de type loi puissance
/// \~french @details    	Constructeur de noyaux analytiques proportionnels à la puissance du nombre de particules élémentaires dans la classe concernée (modèle physique généralisé \f$ k_{i,j} = 0, {k_{i,j}}^* = k*(i+j)^n \f$ )

/// \~english @brief      	Object : Power law breakage kernel
/// \~english @details    	Constructor for analytic kernel, with kinetic coefficients proportional to the power of broken clusters sizes (as a number of unitary clusters) (abstract theoretical model \f$ k_{i,j} = 0, {k_{i,j}}^* = k*(i+j)^n \f$ )
class noyauDislPuis : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		n			Exposant
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param   	 	k			Multiplicative factor
	/// \~english @param    	n			Power law exponent
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauDislPuis (double k, double n, int taille);
};


/// \~french @brief      	Objet : Noyau de fragmentation pure modèle de type loi exponentielle (première variante)
/// \~french @details    	Constructeur de noyaux modèles-types proportionnels à l'inverse de l'exponentielle de l'inverse du nombre de particules élémentaires dans la classe concernée (modèle physique généralisé \f$ k_{i,j} = 0, {k_{i,j}}^* = k \cdot e^{-l \cdot (\frac {1}{i+j})^n} \f$ )

/// \~english @brief      	Object : Exponential-type breakage kernel (first variant)
/// \~english @details    	Constructor for first type breakage exponential model kernel, with kinetic coefficients given by \f$ k_{i,j} = 0, {k_{i,j}}^* = k \cdot e^{-l \cdot (\frac {1}{i+j})^n} \f$
class noyauDislModeleExp1 : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		l			Facteur multiplicatif interne de l'exponentielle
	/// \~french @param    		n			Exposant
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	k			Multiplicative factor
	/// \~english @param    	l			Multiplicative factor for exponential part of the model
	/// \~english @param    	n			Power law exponent
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauDislModeleExp1 (double k, double l, double n, int taille);
};


/// \~french @brief      	Objet : Noyau de fragmentation pure modèle de type loi exponentielle (seconde variante)
/// \~french @details    	Constructeur de noyaux modèles-types proportionnels à l'inverse de l'exponentielle de l'inverse du nombre de particules élémentaires dans la classe concernée (modèle physique généralisé \f$ k_{i,j} = 0, {k_{i,j}}^* = k \cdot (i+j)^{-2/9} \cdot e^{-l \cdot (\frac {1}{i+j})^n} \f$ )

/// \~english @brief      	Object : Exponential-type breakage kernel (second variant)
/// \~english @details    	Constructor for second type breakage exponential model kernel, with kinetic coefficients given by \f$ k_{i,j} = 0, {k_{i,j}}^* = k \cdot (i+j)^{-2/9} \cdot e^{-l \cdot (\frac {1}{i+j})^n} \f$
class noyauDislModeleExp2 : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		l			Facteur multiplicatif interne de l'exponentielle
	/// \~french @param    		n			Exposant
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	k			Multiplicative factor
	/// \~english @param    	l			Multiplicative factor for exponential part of the model
	/// \~english @param    	n			Power law exponent
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauDislModeleExp2 (double k, double l, double n, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure modèle de type brownien
/// \~french @details    	Constructeur de noyaux modèles-types (modèle physique généralisé \f$ k_{i,j} = \frac {k \cdot (i^{1/3}+j^{1/3})^2}{i^{1/3} \cdot j^{1/3}}, {k_{i,j}}^* = 0 \f$ )

/// \~english @brief      	Object : Brownian-type aggregation kernel 
/// \~english @details    	Constructor for brownian-type aggregation model kernel, with kinetic coefficients given by \f$ k_{i,j} = \frac {k \cdot (i^{1/3}+j^{1/3})^2}{i^{1/3} \cdot j^{1/3}}, {k_{i,j}}^* = 0 \f$
class noyauAggModeleBrown : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	k			Multiplicative factor
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauAggModeleBrown (double k, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure modèle de type turbulent (première variante)
/// \~french @details    	Constructeur de noyaux modèles (modèle physique généralisé \f$ k_{i,j} =k \cdot (i^{1/3}+j^{1/3})^2, {k_{i,j}}^* = 0 \f$ )

/// \~english @brief      	Object : Turbulent-type aggregation kernel (first variant)
/// \~english @details    	Constructor for first type turbulent aggregation model kernel, with kinetic coefficients given by \f$ k_{i,j} =k \cdot (i^{1/3}+j^{1/3})^2, {k_{i,j}}^* = 0 \f$
class noyauAggModeleTurb1 : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	k			Multiplicative factor
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauAggModeleTurb1 (double k, int taille);
};


/// \~french @brief      	Objet : Noyau d'aggregation pure modèle de type turbulent (seconde variante)
/// \~french @details    	Constructeur de noyaux modèles (modèle physique généralisé \f$ k_{i,j} =k \cdot (i^{1/3}+j^{1/3})^3,{k_{i,j}}^* = 0 \f$)

/// \~english @brief      	Object : Turbulent-type aggregation kernel (second variant)
/// \~english @details    	Constructor for second type turbulent aggregation model kernel, with kinetic coefficients given by \f$ k_{i,j} =k \cdot (i^{1/3}+j^{1/3})^3,{k_{i,j}}^* = 0 \f$)
class noyauAggModeleTurb2 : public noyau
{
	public:
	/// \~french @brief      	Constructeur à partir de données numériques
	/// \~french @param    		k			Facteur multiplicatif
	/// \~french @param    		taille			Nombre de classes explicitées (à partir des particules unitaires)

	/// \~english @brief      	Constructor from model and numerical parameters
	/// \~english @param    	k			Multiplicative factor
	/// \~english @param    	taille			Kernel size (starting from unitary clusters)
	noyauAggModeleTurb2 (double k, int taille);
};
#endif
