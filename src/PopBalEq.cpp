//Calcul de bilan de population : d'un état initial jusqu'à l'équilibre avec conservation de la matière

#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include "Noyau.h"
#include "Distribution.h"

using namespace std;



int main(int argc, char* argv[])
{
	// Argument 1 : état initial (fichier)
	// Argument 2 : noyau agrégation (fichier)
	// Argument 3 : noyau fragmentation (fichier)
	// Argument 4 : fichier sortie (fichier)
	// Argument 5 : pas de temps
	// Argument 6 : distance de convergence
	// Argument 7 : fréquence d'enregistrement sortie

	// Vérification nombre + conversions types arguments
	if (argc!=8)
	{
		cout << "Mauvais nombre d'arguments" << endl;
		return 1;
	}
	double pas = atof(argv[5]);
	double distance = atof(argv[6]);
	double frequence = atof(argv[7]);
	
	// Suivi des intervalles de sauvegarde
	double t2=0;
	double t=0;
	// Suivi du temps total de simulation
	double t1=0;	
	
	// Construction du noyau 
	noyau kernel(argv[2],argv[3]);

	// Création de la distribution initiale (x2 et une première boucle pour initialiser la distance entre 2 pas de temps) 
	distribution dis(argv[1]);
	distribution dis2(argv[1]);
	t = dis.incremente(&kernel,pas);
	t1=t1+t;
	t2=t2+t;
	
	// Boucle du bilan (tant que la différence entre 2 pas de temps n'atteint pas la distance critique)
	for (;dis.distance(&dis2, "Bhattacharyya") > distance;)
	{
		dis2.incremente(&kernel,pas);
		t = dis.incremente(&kernel,pas);
		t1=t1+t;
		t2=t2+t;
		
		// Export de la distribution tous les pas de temps voulus (ou au plus proches)
		if (t2 >= frequence)
		{
			dis.ecrire(argv[4],t1);
			t2=0;
		}
	}

    	return 0;
 }