//Générateur de noyaux pour bilan de populations : export sous forme de fichiers texte

#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include "Noyau.h"
#include "Distribution.h"

using namespace std;



int main(int argc, char* argv[])
{
	// Argument 1 : type noyau agrégation
	// Argument 2 : type noyau fragmentation
	// Argument 3 : noyau agrégation (fichier)
	// Argument 4 : noyau fragmentation (fichier)
	// Argument 5 : taille

	int taille = atoi(argv[5]);
	double k, r, nu, epsilon;
	int i;

	// Vérification nombre + conversions types arguments
	if (argc!=6)
	{
		cout << "Mauvais nombre d'arguments" << endl;
		return 1;
	}

	
	// Test des types possibles pour agrégation puis fragmentation
	for (i=1;i!=3;i++)
	{
		if (std::string(argv[i]) == "produit")
		{
			cout << " Coefficient k pour kij = k.(i.j) : ";
			cin >> k;
			noyauAggProd noyagg(k, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "somme")
		{
			cout << " Coefficient k pour kij = k.(i+j) : ";
			cin >> k;
			noyauAggSom noyagg(k, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "campstein")
		{
			cout << " Noyau agrégation Camp-Stein kij = k.(i+j)" << endl;
			cout << " Rayon de particule unitaire r : ";
			cin >> r;
			cout << " Viscosité cinématique nu : ";
			cin >> nu;
			cout << " Vitesse de dissipation d'énergie fluide massique epsilon : ";
			cin >> epsilon;
			noyauCampStein noyagg(r, nu, epsilon, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "constante")
		{
			cout << " Coefficient k pour kij = k : ";
			cin >> k;
			noyauConst noyagg(k, 0, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "decantation")
		{
			cout << " Noyau agrégation decantation kij = k.(i+j)" << endl;
			cout << " Rayon de particule unitaire r : ";
			cin >> r;
			cout << " Viscosité dynamique mu : ";
			cin >> nu;
			cout << " Masse volumique des particules rho : ";
			cin >> epsilon;
			noyauDecant noyagg(r, nu, epsilon, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "levich")
		{
			cout << " Noyau agrégation Levich kij = k.(i+j)" << endl;
			cout << " Rayon de particule unitaire r : ";
			cin >> r;
			cout << " Viscosité cinematique nu : ";
			cin >> nu;
			cout << " Vitesse de dissipation d'énergie fluide massique epsilon : ";
			cin >> epsilon;
			noyauLevich noyagg(r, nu, epsilon, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "saffmanturner")
		{
			cout << " Noyau agrégation Saffman-Turner kij = k.(i+j)" << endl;
			cout << " Rayon de particule unitaire r : ";
			cin >> r;
			cout << " Viscosité cinematique nu : ";
			cin >> nu;
			cout << " Vitesse de dissipation d'énergie fluide massique epsilon : ";
			cin >> epsilon;
			noyauSaffTurner noyagg(r, nu, epsilon, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "smolubrown")
		{
			cout << " Noyau agrégation Smoluchowski brownien kij = k.(i+j)" << endl;
			cout << " Rayon de particule unitaire r : ";
			cin >> r;
			cout << " Tepérature T : ";
			cin >> nu;
			cout << " Viscosité cinematique mu : ";
			cin >> epsilon;
			noyauSmolBrown noyagg(r, nu, epsilon, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
		if (std::string(argv[i]) == "smolulam")
		{
			cout << " Noyau agrégation Smoluchowski laminaire kij = k.(i+j)" << endl;
			cout << " Rayon de particule unitaire r : ";
			cin >> r;
			cout << " Vitesse de cisaillement v : ";
			cin >> nu;
			noyauSmolLam noyagg(r, nu, taille); 
			noyagg.ecrire(argv[i+2],1);
		}
	}
	
    	return 0;
 }