//Calcul de bilan de population : comparaison de points de départ multiples

#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include "Noyau.h"
#include "Distribution.h"
#include <omp.h>
#include <algorithm>
using namespace std;



// Création d'un noyau agg-frag de type donné à partir d'une liste de paramètres
noyau * CreationNoyau(vector <double>  listeparametres, string modeleagg, string modeledisl, int nbclasses)
{
	noyau * NoyauTh;
	noyau * NoyauTh2;

	// Modèles d'agrégation
	// Brownien
	if (modeleagg=="brown")
	{
		NoyauTh = new noyauAggModeleBrown (1, nbclasses);
	}
	// Turbulent type 1
	if (modeleagg=="turb1")
	{
		NoyauTh = new noyauAggModeleTurb1 (1, nbclasses);
	}
	// Turbulent type 2
	if (modeleagg=="turb2")
	{
		NoyauTh = new noyauAggModeleTurb2 (1, nbclasses);
	}

	// Modèles de dislocation
	// Loi puissance
	if (modeledisl=="puis")
	{
		NoyauTh2 = new noyauDislPuis (listeparametres[1], listeparametres[2], nbclasses);
	}
	// Loi exponentielle type 1
	if (modeledisl=="exp1")
	{
		NoyauTh2 = new noyauDislModeleExp1 (listeparametres[1], listeparametres[2], listeparametres[3], nbclasses);
	}
	// Loi exponentielle type 2
	if (modeledisl=="exp2")
	{
		NoyauTh2 = new noyauDislModeleExp2 (listeparametres[1], listeparametres[2], listeparametres[3], nbclasses);
	}

	// Ajout agrégation / fragmentation
	NoyauTh->ajout(NoyauTh2);
	delete NoyauTh2;
	return NoyauTh;
}


// Tri des vecteur du simplexe par le premier terme (correspond au critère, les autres sont les coordonnées/variables du modèle de noyau)
bool triSimplexe (const vector <double>& lhs, const vector <double>& rhs) { return lhs[0] < rhs[0]; }


int main(void)
{
	// Boucles
	int i,j;

	// Timer
	clock_t t1,t2;


	ofstream fichier;
	char nom[50];

	// Nombre de classes
	int N;

	// Types de modèle de noyaux
	string noyAgg;
	string noyDisl;

	// Nombre d'états d'équilibre à analyser
	int NB;

	// Liste des distributions (correspondent à différents états initiaux)
	vector<distribution> liste;
	// Contrôle parallélisation (optionnel)
	int nb_process=1;
	// Noyaux utilisés
	noyau * NoyauTh;

	// Variables du noyau théorique (nombre + liste)
	int nbVariables;
	// Stockage des coordonnées du simplexe +  valeurs de critère correspondantes (initialisation à 0)
	vector <vector <double>> variables;
	// Coordonnées + critère des points transitoires 
	vector <double> centre;
	vector <double> reflechi;
	vector <double> expanse;

	// Critère d'arrêt de l'optimisation
	double limitecritere=0.00000000000001;
	double test=1;
	
	// Pas pour la construction du simplexe de départ
	double pasvar;



	// Enregistrement de durée de calcul
    	t1=clock();


	// Activation du parallélisme
	//#pragma omp parallel num_threads(nb_process) private(i,j,k,nom,liste,fichier,c,N,NB)
	{	

		// Récupération des données nécessaires au fit
		cout << endl << "Bilan de populations : depouillement a partir d'etats d'equilibre methode Nelder-Mead " << endl << endl;
		cout << "Taille du noyau (taille maximale de clusters) : " << endl;
    		cin >> N;
		cout << "Nombre d'etats d'equilibre a analyser (fichiers experimentaux en colonne - Data1.txt - Data2.txt...) : " << endl;
    		cin >> NB;
		cout << "Modele d'agregation (brown, turb1 ou turb2) : " << endl;
    		cin >> noyAgg;
		cout << "Modele de fragmentation (puis, exp1 ou exp2) : " << endl;
    		cin >> noyDisl;

		// Détermination du nombre de variables du noyau théorique
		if (noyDisl=="puis")
		{
			nbVariables=2;
		}
		else
		{
			nbVariables=3;			
		}

		// Construction du simplexe de départ
		for (i=0;i!=nbVariables+1;i++)
		{
			variables.push_back(vector<double> (nbVariables+1,0));
			centre.push_back(0);
			reflechi.push_back(0);
			expanse.push_back(0);
		}

		// Coordonnées des points de départ du simplexe
		for (i=0;i!=nbVariables;i++)
		{
			cout << "Coordonnees de depart de l'optimisation x" << i+1 << " :" << endl;
			cin >> variables[0][i];
		}
		cout << "Pas des variables initiales (sera ajoute a chaque variable pour definir le simplexe de depart) : " << endl;
    		cin >> pasvar;

		// Points autour de l'initial
		for (i=1;i!=nbVariables+1;i++)
		{
			for (j=1;j!=nbVariables+1;j++)
			{
				// Variable selon laquelle on ajoute le pas
				if (i==j)
				{
					variables[i][j]=variables[0][j]+pasvar;
				}
				// Les autres variables sont identiques à l'état central
				else
				{
					variables[i][j]=variables[0][j];
				}
			}
		}

		// Ouverture des fichiers contenant les états d'équilibre
		for (i=0;i!=NB;i++)
		{
			sprintf(nom, "Data%d.txt", i+1);
			liste.push_back(distribution(nom));
		}

		// Calcul du critère correspondant à chaque point du simplexe
		for (i=0;i!=nbVariables+1;i++)
		{
			// Création noyau pour chaque point du simplexe
			NoyauTh = CreationNoyau(variables[i], noyAgg, noyDisl, N);

			// Somme des critères élémentaires pour chaque état d'équilibre
			for (j=1;j!=NB+1;j++)
			{
				variables[i][0]=variables[i][0]+liste[j-1].critcompat(NoyauTh);
			}	
			delete NoyauTh;	
		}

		sort(variables.begin(), variables.end(), triSimplexe);

		// Fin d'initialisation


		// Algorithme proprement dit (conditions d'arrêt : critère inférieur à un seuil / critère sans évolution pendant 10 réductions générales successives)

		while(test>limitecritere)
		{		

			// 2-Calcul du centre de gravité des points du simplexe, à l'exception du plus mauvais du point de vue du critère
			for (i=1;i!=nbVariables+1;i++)
			{
				// Initialisation
				centre[i]=0;

				// Calcul de la moyenne, à l'exception du plus mauvais du point de vue du critère (le dernier)
				for (j=0;j!=nbVariables;j++)
				{
					centre[i]=centre[i]+variables[j][i]/(nbVariables);
				}		
			}


			// 3-Réflexion par le centre de gravité du plus mauvais point
			for (i=1;i!=nbVariables+1;i++)
			{
				reflechi[i]=2*centre[i]-variables[nbVariables][i];
			}
			// Calcul du critère en ce point réfléchi (nécessite le calcul du noyau correspondant)
			reflechi[0]=0;
			NoyauTh = CreationNoyau(reflechi, noyAgg, noyDisl, N);
			// Somme des critères élémentaires pour chaque état d'équilibre
			for (j=1;j!=NB+1;j++)
			{
				reflechi[0]=reflechi[0]+liste[j-1].critcompat(NoyauTh);
			}	
			delete NoyauTh;	


			// 4-Si le point réfléchi est compris entre le plus mauvais et le meilleur selon le critère, on remplace le point le plus mauvais par le réfléchi
			if (reflechi[0]<variables[nbVariables][0] && reflechi[0]>=variables[0][0] )
			{
				for (i=0;i!=nbVariables+1;i++)
				{
					variables[nbVariables][i]=reflechi[i];
				}
			}


			// 5-Si le point réfléchi est meilleur que le meilleur, on recherche un point 2 fois plus loin dans cette direction (point expansé)
			else if (reflechi[0]<variables[0][0] )
			{
				for (i=1;i!=nbVariables+1;i++)
				{
					expanse[i]=2*reflechi[i]-centre[i];
				}
				// Calcul du critère en ce point expansé (nécessite le calcul du noyau correspondant)
				expanse[0]=0;
				NoyauTh = CreationNoyau(expanse, noyAgg, noyDisl, N);
				// Somme des critères élémentaires pour chaque état d'équilibre
				for (j=1;j!=NB+1;j++)
				{
					expanse[0]=expanse[0]+liste[j-1].critcompat(NoyauTh);
				}	
				delete NoyauTh;
			
				// Si le point expansé est meilleur que le réfléchi, l'expansé remplace le plus mauvais point
				if (expanse[0]<=reflechi[0])
				{

					for (i=0;i!=nbVariables+1;i++)
					{
						variables[nbVariables][i]=expanse[i];
					}
				}
				// Sinon, le réfléchi remplace le plus mauvais point
				else
				{

					for (i=0;i!=nbVariables+1;i++)
					{
						variables[nbVariables][i]=reflechi[i];
					}				
				}
			}

		
			// 6-Si le point réfléchi est plus mauvais que tous les autres, on opère une réduction de ce dernier (on utilise le tableau expanse pour economiser la memoire) 
			else
			{
				for (i=1;i!=nbVariables+1;i++)
				{
					expanse[i]=0.5*centre[i]+0.5*variables[nbVariables][i];
				}
				// Calcul du critère en ce point réduit (nécessite le calcul du noyau correspondant)
				expanse[0]=0;
				NoyauTh = CreationNoyau(expanse, noyAgg, noyDisl, N);
				// Somme des critères élémentaires pour chaque état d'équilibre
				for (j=1;j!=NB+1;j++)
				{
					expanse[0]=expanse[0]+liste[j-1].critcompat(NoyauTh);
				}	
				delete NoyauTh;			
				// Si le point réduit est meilleur que le réfléchi, le réduit remplace le plus mauvais point
				if (expanse[0]<reflechi[0])
				{
					for (i=0;i!=nbVariables+1;i++)
					{
						variables[nbVariables][i]=expanse[i];
					}
				}
				// Sinon, on opère une réduction de tous les points sauf du meilleur
				else
				{
					// Pour tous les points sauf le meilleur
					for (i=1;i!=nbVariables+1;i++)
					{
						// Réduction (coordonnées)
						for (j=1;j!=nbVariables+1;j++)
						{
							variables[i][j]=0.5*variables[0][j]+0.5*variables[i][j];
						}

						// Calcul du critère en ce point réduit (nécessite le calcul du noyau correspondant)
						variables[i][0]=0;
						NoyauTh = CreationNoyau(variables[i], noyAgg, noyDisl, N);
						// Somme des critères élémentaires pour chaque état d'équilibre
						for (j=1;j!=NB+1;j++)
						{
							variables[i][0]=variables[i][0]+liste[j-1].critcompat(NoyauTh);
						}	
						delete NoyauTh;
					}
				}
			}

			// 1-Tri par valeur de critère
			sort(variables.begin(), variables.end(), triSimplexe);

			// Critere d'arret (identité entre le sommet le meilleur et le plus mauvais -> points deviennent inclassables)
			test=0;
        		test=abs(variables[0][0]-variables[nbVariables][0]);	


		}
		// Fin de l'optimisation	

		// Ecriture des variables obtenues
		cout << endl << "Seuil atteint" << endl << "Variables :" << endl;
		for (i=1;i!=nbVariables+1;i++)
		{
			cout << "x" << i << " = " << variables[0][i] << endl;
		}
		cout << "Critere = " << variables[0][0] << endl;		

	}

	// Suivi des durées de calcul
	t2=clock();
    	float diff ((float)t2-(float)t1);
   	cout << endl << "Duree = " << diff/(CLOCKS_PER_SEC*nb_process) << "s" << endl;
    	return 0;
}