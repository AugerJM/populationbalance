///\~french @file      	DistributionQuant.h
///\~french @author    	JMA
///\~french @version   	2.0
///\~french @date      	15/05/2019
///\~french @brief     	Définit une distribution 
///\~french @details   	Cette classe définit une distribution de nombre de particules selon leur taille				 

///\~english @file      	DistributionQuant.h
///\~english @author    	JMA
///\~english @version   	2.0
///\~english @date      	15/05/2019
///\~english @brief     	Defines a distribution 
///\~english @details   	This class defines a distribution of clusters numbers by clusters size	

#ifndef DISTRIBUTIONQUANT_H
#define DISTRIBUTIONQUANT_H

#include <string>
#include <vector>
#include "Noyau.h"

class noyau;

/// \~french @brief      Objet : Distribution en nombre 
/// \~french @details    Contient les nombres des différentes classes d'agrégats, ainsi que les opérations standard.

/// \~english @brief      Object : Clusters numbers distribution
/// \~english @details    Contains number for varying cluster sizes, and standard operations.

class distributionquant
{
	private:
	
	/// \~french @brief 	 	Nombre de classes définies dans le noyau (en partant de la classe de particules unitaires).
	/// \~french @details    	Les coefficients correspondant à des classes de supérieures sont considérées nuls (classes non réactives).

	/// \~english @brief 	  	Number of kernel cluster classes (starting from unitary cluster).
	/// \~english @details   	Concentrations of bigger clusters are considered egal to 0 (non-reactive cluster classes).
	int nbclasse;

	/// \~french @brief 	Volume total du milieu.

	/// \~english @brief 	Total simulated medium volume.
	double volume;

	/// \~french @brief 		Tableau de la distribution (en nombre d'agrégats d'une taille donnée)
	/// \~english @brief 		Distribution table (number of clusters of a given size)
	std::vector<int> distrib;



	public:
	
	/// \~french @brief      	Constructeur de base

	/// \~english @brief     	Base constructor
	distributionquant (void);
	

	/// \~french @brief     	Constructeur de la distribution à partir d'un fichier
	/// \~french @details   	Les valeurs sont séparées par des espaces, et commencent à la classe unitaire. La première ligne contient le volume du milieu. Note : permet de reprendre un calcul sauvegardé. 
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Distribution constructor from file
	/// \~english @details    	Values are separated by spaces, and begin from unitary clusters. First line is the medium volume. Note : allows to restart a stored computation. 
	/// \~english @param    	nom		File name
	distributionquant (std::string nom);


	/// \~french @brief      	Constructeur de copie de la distribution
	/// \~french @param    		dis		Pointeur vers la distribution à copier

	/// \~english @brief     	Copy constructor for distribution
	/// \~english @param    	dis		Pointer to distribution to copy
	distributionquant (const distributionquant &dis);


	/// \~french @brief      	Ecriture de la distribution à l'écran
	/// \~english @brief      	Writes distribution on screen
	void ecrire () const;


	/// \~french @brief      	Ecriture de la distribution dans un fichier
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par le volume du milieu, puis la classe des particules unitaires.
	///				Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Writes distribution in file
	/// \~english @details    	Values are written in lines, starting from medium volume, then number of unitary unitary clusters.
	///				Note : file is open in append mode.
	/// \~english @param    	nom		Filename
	void ecrire (std::string nom) const;


	/// \~french @brief      	Ecriture de la distribution dans un fichier (variante avec enregistement du pas de temps)
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par le volume du milieu, puis la classe de particules unitaires. La première valeur correspond au pas de temps (utile en cas de pas de temps variable).
	///			    	Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		nom		Nom du fichier
	/// \~french @param    		temps		Pas de temps à indiquer

	/// \~english @brief      	Writes distribution in file (variant including time step information)
	/// \~english @details    	Concentrations are written in lines, starting from medium volume, then number of unitary unitary clusters. First value in each line corresponds to time step (useful for variable time steps).
	///			    	Note : file is open in append mode.
	/// \~english @param    	nom		Filename
	/// \~english @param    	temps		Timestep to append	
	void ecrire (std::string nom, double temps) const;


	/// \~french @brief      	Affichage du nombre total de particules élémentaires de la distribution à l'écran

	/// \~english @brief      	Writes the distribution total unitary clusters number on screen
	void population (void) const;


	/// \~french @brief      	Renvoie la population totale en particules de la distribution
	/// \~french @details    	Utilisé en interne pour ajuster les calculs de distances entre distributions.
	/// \~french @return    	Nombre total de particules élémentaires

	/// \~english @brief      	Returns the distribution total unitary clusters number
	/// \~english @details    	Used in internal computations involving distribution distances.
	/// \~english @return    	Total unitary clusters number in the distribution
	double populationval (void) const;


	/// \~french @brief      	Calcul de la distribution au pas de temps suivant (met à jour la distribution); 
	/// \~french @details    	Les particules sont agrégées ou fragmentées une par une (approche quantifiée). Note : pour obtenir un avancement représentatif, le pas de temps doit être supérieur au plus grand temps caractéristique des réaction élémentaires.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour le calcul d'avancement
	/// \~french @param    		tps		Temps (durée en s)
	/// \~french @return    	Temps effectivement simulé (durée juste supérieure au temps imposé conduisant à l'avancement d'un équilibre élémentaire quelconque, en s)

	/// \~english @brief      	Advancement to next timestep (updates distribution);
	/// \~english @details    	Clusters are aggregated or broken one by one (quantified approach). Note : in order to have a realistic time advance, the time step must exceed the greatest of characteristic times for elementary reactions.
	/// \~english @param    	noy		Pointer to kernel used during this timestep
	/// \~english @param    	tps		Time (duration in s)
	/// \~english @return    	Effective computed time (duration to the first elementary advancement following the chosen time, in s) 	
	double incremente (noyau * noy, double tps);


	/// \~french @brief      	Calcul de la distribution après transfert d'un nombre fixé de paritcules entre les classes (met à jour la distribution);
	/// \~french @details    	Les particules sont agrégées ou fragmentées une par une (approche quantifiée). Note : pour obtenir un avancement représentatif, le pas de temps doit être supérieur au plus grand temps caractéristique des réaction élémentaires.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour le calcul d'avancement
	/// \~french @param    		nb		Nombre de particules transférées
	/// \~french @return    	Temps effectivement simulé (durée en s)

	/// \~english @brief      	Distribution computation after a given number of cluster transferred (updates distribution);
	/// \~english @details    	Clusters are aggregated or broken one by one (quantified approach). Note : in order to have a realistic time advance, the time step must exceed the greatest of characteristic times for elementary reactions.
	/// \~english @param    	noy		Pointer to kernel used during this timestep
	/// \~english @param    	nb		Number of clusters transferred
	/// \~english @return    	Effective computed time (duration in s) 	
	double incremente (noyau * noy, int nb);


	/// \~french @brief      	Renvoie la concentration en particules de la classe i
	/// \~french @param    		i		Classe de particules (nombre de particules élémentaires par particule, >0)
	/// \~french @return    	Concentration (en nombre d'agrégats par  volume) dans la classe i

	/// \~english @brief      	Returns the concentration of size i clusters
	/// \~english @param    	i		Cluster size (number of unitary particles in cluster, >0)
	/// \~english @return    	Concentration (number per volume) of size i clusters
	double element (int i) const;


	/// \~french @brief      	Renvoie le nombre de classes dans la distribution
	/// \~french @return    	Nombre total de classes (>0)

	/// \~english @brief      	Return the maximal clusters size in distribution
	/// \~english @return    	Maximal clusters size (unitary clusters, >0)
	int taille (void) const;


	/// \~french @brief      	Ajout d'une autre distribution
	/// \~french @details    	L'ajout se fait à la distribution en cours (l'opération ne crée pas de nouvelle distribution-somme). Lors de l'opération, la distribution est redimensionnée au plus grand nombre de classes des 2 distributions ajoutées. 
	/// \~french @param    		distr2		Pointeur vers la distribution à ajouter

	/// \~english @brief      	Adds another distribution
	/// \~english @details    	The other distribution is added to the current one (the resulting sum is not created as a third distribution). During this operation, the distribution is resized to correspond to the greater clusters size from the 2 summed distributions.
	/// \~english @param    	distr2		Pointer the distribution to add to the current one
	void ajout (distributionquant * distr2);


	/// \~french @brief      	Calcul de distances entre 2 distributions
	/// \~french @details    	Plusieurs définitions de la distance sont disponibles via le paramètre type.  
	/// \~french @param    		distr2		Pointeur vers la distribution dont la distance est à mesurer	
	/// \~french @param    		type		Indicateur de la distance utilisée ("TVD" : Total Variation Distance - "Hellinger" : Hellinger - "Bhattacharyya" : Bhattacharyya)

	/// \~english @brief      	Computes the distance between 2 distributions
	/// \~english @details    	Various distance definitions are usable through the "type" parameter.  
	/// \~english @param    	distr2		Pointer to the distribution with which the distance must be calculated	
	/// \~english @param    	type		Indicator of the distance definition used ("TVD" : Total Variation Distance - "Hellinger" : Hellinger - "Bhattacharyya" : Bhattacharyya) 
	double distance (distributionquant * distr2, std::string type) const;


	/// \~french @brief      	Calcul et affichage à l'écran des flux de matière correspondant aux équilibres élémentaires
	/// \~french @details    	Les valeurs obtenues sont fortement dépendantes du noyau passé en paramètre. Le pas de temps est normalisé à 1.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les flux

	/// \~english @brief      	Computation of instant matter flux corresponding to each elementary aggregation/fragmentation equilibrium, which are then written on screen
	/// \~english @details    	Values highly dependent on the kernel used for computation. Time step is normalized to 1.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	void flux (noyau * noy) const;


	/// \~french @brief      	Ecriture des flux de matière correspondant aux équilibres élémentaires dans un fichier
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par les flux entre particules les plus petites.
	///			    	Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Computation of instant matter flux corresponding to each elementary aggregation/fragmentation equilibrium, which are then written in a file
	/// \~english @details    	Values are written in lines, starting from equilibirums involving the smaller clusters.
	///			    	Note : file is open in append mode.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~english @param    	nom		Filename
	void flux (noyau * noy, std::string nom) const;


	/// \~french @brief      	Dilution/concentration du milieu représenté par la distribution
	/// \~french @details    	Multiplication des concentrations de la distribution par une constante.
	/// \~french @param    		facteur		Facteur mutiplicatif appliqué (>1 - concentration, <1 - dilution)

	/// \~english @brief      	Applies a dilution or concentration increase in the medium simulated by the distribution
	/// \~english @details    	Multiplies all distribution concentrations by the same constant.
	/// \~english @param    	facteur		Applied multiplicative constant (>1 - concentration increase, <1 - dilution)
	void dilution (float facteur);


	/// \~french @brief      	Calcul et affichage à l'écran des constantes d'équilibres normalisées des réactions élémentaires
	/// \~french @details    	Les valeurs obtenues sont fortement dépendantes du noyau passé en paramètre.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre

	/// \~english @brief      	Computation of normalized equilibrium constants corresponding to each elementary aggregation/fragmentation, which are then written on screen
	/// \~english @details    	Values highly dependent on the kernel used for computation. Time step is normalized to 1.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	void equilibres (noyau * noy) const;


	/// \~french @brief      	Ecriture des constantes d'équilibres normalisées des réactions élémentaires dans un fichier
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par les constantes d'équilibre entre particules les plus petites.
	///			    	Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Computation of normalized equilibrium constants corresponding to each elementary aggregation/fragmentation, which are then written in file
	/// \~english @details    	Values are written in lines, starting from equilibirums involving the smaller clusters.
	///			    	Note : file is open in append mode.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~english @param    	nom		Filename
	void equilibres (noyau * noy, std::string nom) const;


	/// \~french @brief     	Calcul d'un critère de stabilité de la distribution avec un noyau donné
	/// \~french @details    	Le critère actuel sert pour des tests (vérification de sa validité). Il est positif et ne s'annule qu'à l'équilibre
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @return    	Valeur du critère

	/// \~english @brief      	Computation of a stability criterion for the distribution evolution under a given kernel
	/// \~english @details    	Criterion is under tests (validity). Values are positive, and reach zero only when the distribution is balanced under a given kernel
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~french @return    	Criterion value
	double critere (noyau * noy) const;


	/// \~french @brief      	Effet de l'avancement d'une réaction élémentaire sur la distribution ( i + j -> (i+j) )
	/// \~french @details    	Pas de sécurité sur cette méthode (les concentrations peuvent passer en négatif). Utilité : tester le voisinage de la distribution tout an ayant conservation de la matière
	/// \~french @param    		i		Première taille de cluster de l'équilibre à avancer
	/// \~french @param    		j		Seconde taille de cluster de l'équilibre à avancer
	/// \~french @param    		av		Avancement imposé (en particules par unité de volume, peut être négatif)

	/// \~english @brief      	Displaces the elementary aggregation reaction ( i + j -> (i+j) )
	/// \~english @details    	This method is not secured (concnetrations can reach negative values). Uses : easy access to the distribution neighbourhood while still have unitary clusters conservation
	/// \~english @param    	i		First clusters size involved in the reaction to advance
	/// \~english @param    	j		Second clusters size involved in the reaction to advance
	/// \~english @param    	av		Displacement (clusters number per volume, can be negative)
	void avancement (int i, int j, int av);


	/// \~french @brief      	Condition limite de type Neumann (impose une concentration dans une classe) - renvoie le flux de matière instantané nécessaire pour équilibrer la classe
	/// \~french @details    	Permet de produire un bilan de population plus général (sans conservation de la matière - équilibre en flux)
	///				Si le numéro de classe demandé est impossible, la condition est affectée à la classe viable la plus proche (donc la plus grande ou la plus petite)
	///			    	Note : l'utilisation dans le cadre d'un bilan impose d'alterner l'incrémentation avec une ou plusieurs conditions aux limites
	/// \~french @param    		i		Taille de cluster dont la concentration est imposée
	/// \~french @param    		val		Concentration imposée
	/// \~french @return    	Nombre d'agrégats par unité de volume à ajouter pour assurer la condition (peut être négatif)

	/// \~english @brief      	Neumann condition (forces concentration of one cluster size in distribution) - returns the instant clusters flux needed to balance out clusters concentration
	/// \~english @details    	Allows more general population balance computations (without matter conservation - balanced in flux)
	///				If the cluster size affected does not exist in the distribution, Neumann condition is applied to the nearest one (either the biggest or smallest clusters)
	///			    	Note : use during population balance computation involves alternating time incrementation with the appliation of one or more limit conditions 
	/// \~english @param    	i		Cluster size which concentration is to be imposed
	/// \~english @param    	val		Concentration imposed
	/// \~english @return    	Number of aggregates per volume to add to cluster concentration in order to apply the condition
	double neumann (int i, double val);


	/// \~french @brief      	Calcul et renvoi d'un critère valant 0 quand le noyau passé en argument est compatible avec l'état d'équilibre décrit par la distribution
	/// \~french @details    	Les valeurs obtenues sont fortement dépendantes du noyau passé en paramètre. Le pas de temps est normalisé à 1.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les flux
	/// \~french @return    	Valeur du critère (positif ou nul)

	/// \~english @brief      	Returns a positive criterion which value is zero when a kernel is compatible with the distribution as an equilibrium state
	/// \~english @details    	Values highly dependent on the kernel used for computation. Time step is normalized to 1.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~english @return    	Criterion value (positive)
	double critcompat (noyau * noy) const;
};
#endif