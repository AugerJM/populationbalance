#include "DistributionQuant.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <cmath>
#include <algorithm>

using namespace std;

// Objet : distribution de tailles de particules
// Méthodes : 	distribution - construction
//		ecrire - affichage ou sauvegarde de la distribution
//		population - calcul du total de particules élémentaires (vérifie la conservation)
//		incremente - calcul du pas de temps suivant à l'aide d'un noyau d'aggrégation

	
// Nombre de classes
//int nbclasse;
// Tableau de la distribution (à partir de 1 particule)
//vector<int> distrib;


// Fonction de tri des équilibres par temps restants croissants
bool triEq (const vector <double>& lhs, const vector <double>& rhs) { return lhs[3] < rhs[3]; }




// Constructeur de base non nécessaire
	
// Constructeur à partir d'un nom de fichier
distributionquant::distributionquant (std::string nom)
{
	nbclasse=0;
	// Génère la distribution initiale à partir d'un fichier - classe de particule = numéro de ligne-1 (en partant de la particule élémentaire)
	ifstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
     	{    
		string ligne;
		// Première ligne : récupération du volume du milieu 
		getline(fichier,ligne);
		volume=atof(ligne.c_str());
		while (getline(fichier,ligne))
		{
			nbclasse++;
			distrib.push_back(atoi(ligne.c_str()));
		}	
       		fichier.close();
	}
	else
	{
        	cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}

// Constructeur de copie
distributionquant::distributionquant (const distributionquant &dis)
{
	int i;
	nbclasse=dis.nbclasse;
	volume=dis.volume;
	// Copie terme à terme de la distribution
	for (i=0;i!=nbclasse;i++)
	{
		distrib.push_back(dis.distrib[i]);
	}
}

// Destructeur spécifique non nécessaire

// Fonction d'écriture - classe de particule / concentration
// Ecriture à l'écran
void distributionquant::ecrire (void) const
{
	int i;
	cout << "Volume total = " << volume << endl;
	cout << endl << "Taille (particules elementaires)\tNombre" << endl;
	for (i=0;i!=nbclasse;i++)
	{
		cout << i+1 << "\t\t\t\t\t" << distrib[i] << endl;
	}
	cout << endl;
}
// Ecriture dans un fichier
void distributionquant::ecrire (std::string nom) const
{
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
	{    
		int i;
		fichier << volume << " ";
		for (i=0;i!=nbclasse;i++)
		{
			fichier << distrib[i] << " ";
		}
		fichier << endl;
           fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}
// Variante qui enregistre le pas de temps
void distributionquant::ecrire (std::string nom, double temps) const
{
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
     	{    
		int i;
		fichier << temps << " ";
		fichier << volume << " ";
		for (i=0;i!=nbclasse;i++)
		{
			fichier << distrib[i] << " ";
		}
		fichier << endl;
           fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}

// Ecrit la valeur de population totale (nombre total de particules élémentaires)
void distributionquant::population (void) const
{
	int i;
	double somme=0;
	for (i=0;i!=nbclasse;i++)
	{
		somme=somme+distrib[i]*(i+1);
	}
	cout << somme << endl;
}
// Version à usage interne (renvoie la valeur au lieu de l'écrire)
double distributionquant::populationval (void) const
{
	int i;
	double somme=0;
	for (i=0;i!=nbclasse;i++)
	{
		somme=somme+distrib[i]*(i+1);
	}
	return somme;
}

// Passage au pas de temps suivant (noyau, durée d'avancement)
double distributionquant::incremente (noyau * noy, double tps)
{
	// Variables de contrôle
	int i,j,k;
	// Nombre d'équilibres élémentaires (va augmenter quand la distribution s'étend)
	int n=0;
	// Sens de l'équilibre courant
	int ajout;
	// Pas de temps élémentaire jusqu'au prochain transfert de matière (variable)
	double deltat;
	// Temps caractéristique jusqu'au prochain transfert de matière (variable)
	double tcarac;
	// Nombre de classes temporaire
	int nbclassetemp;
	// Suivi du temps total
	double suivi=0;

	// Redimensionner le tableau de classes (en remplissant de 0)
	distrib.resize(nbclasse*2,0);
	nbclasse=nbclasse*2;

	// Préparer liste des états d'équilibres élémentaires (il sont avancés l'un après l'autre, par cinétique décroissante) 
	vector <vector <double>> listeeq;
	for (i=1;i!=nbclasse/2+1;i++)
	{
		for (j=1;j!=i+1;j++)
		{
			listeeq.push_back(vector<double>(4,0));
			// Clusters agrégés dans cet équilibre
			listeeq[n][0]=j;
			listeeq[n][1]=i;
			// Temps caractéristique d'un cluster pour cet équilibre (une valeur négative signifie que la réaction est de type fragmentation) 
			listeeq[n][2]=1/((*noy).element(i,j,1)*distrib[i-1]*distrib[j-1]/volume-(*noy).element(i,j,2)*distrib[i+j-1]);
			// Temps restant avant avancement d'un cluster de l'équilibre (initialement égal au temps caractéristique, diminue avec l'écoulement du temps)
			listeeq[n][3]=abs(listeeq[n][2]);
			n++;
		}
	}

	// Tri des équilibres élémentaires par temps restant croissant
	sort(listeeq.begin(), listeeq.end(), triEq);

	// Tant que le temps imposé n'est pas atteint, ou que le temps caractéristique le plus court n'est pas infini (distribution stable), on applique les transferts de matière
	while (suivi < tps && !isinf(listeeq[0][2]))
	{

		// Le premier équilibre de la liste est celui qui subit un avancement le premier

		// Sens de la réaction : positif (=1) si agrégation, négatif (-1) sinon
		ajout = (int) 1-2*signbit(listeeq[0][2]);
		i=(int) listeeq[0][1];
		j=(int) listeeq[0][0];
		deltat=listeeq[0][3];
		suivi+=listeeq[0][3];

		// Si l'équilibre est une double agrégation (2 clusters identiques), et que la classe ne contient qu'une particule, la réaction est impossible : on double le temps restant (réinitialise le chrono pour cet équilibre)
		if ((ajout>0) && (i == j) && (distrib[i-1] == 1))
		{
			listeeq[0][3]+=abs(listeeq[0][2]);

			// Diminution des temps restants de toutes les réactions en cours
			for (k=0;k!=n;k++)
			{
				listeeq[k][3]-=deltat;
			}
		}

		// Sinon, on avance la réaction
		else
		{

			// Déplacement des clusters dans la distribution
			// Réactifs
			distrib[i-1] -= (double) ajout;
			distrib[j-1] -= (double) ajout;
			// Produit
			distrib[i+j-1] += (double) ajout;

			// Evolution des temps de réactions en cours
			listeeq[0][3]+=abs(listeeq[0][2]);

			for (k=0;k!=n;k++)
			{
				// Diminution des temps restants de toutes les réactions en cours
				listeeq[k][3]-=deltat;

				if (listeeq[k][0] == i || listeeq[k][0] == j || listeeq[k][1] == i || listeeq[k][1] == j || listeeq[k][0] == i+j || listeeq[k][1] == i+j || listeeq[k][0]+listeeq[k][1] == j || listeeq[k][0]+listeeq[k][1] == i || listeeq[k][0]+listeeq[k][1] == j+i)
				{

					if (i==j)
					{
						tcarac=1/((*noy).element(listeeq[k][1],listeeq[k][0],1)/volume*distrib[listeeq[k][0]-1]*(distrib[listeeq[k][1]-1]-1)-(*noy).element(listeeq[k][1],listeeq[k][0],2)*distrib[listeeq[k][0]+listeeq[k][1]-1]);
					}
					else
					{
						tcarac=1/((*noy).element(listeeq[k][1],listeeq[k][0],1)/volume*distrib[listeeq[k][0]-1]*distrib[listeeq[k][1]-1]-(*noy).element(listeeq[k][1],listeeq[k][0],2)*distrib[listeeq[k][0]+listeeq[k][1]-1]);
					}
					

					// Correction du temps restant
					if (isinf(listeeq[k][2]))
					{
						listeeq[k][3]=abs(tcarac);
					}
					else
					{
						listeeq[k][3]=(listeeq[k][3]/abs(listeeq[k][2]))*abs(tcarac);
					}

					// Nouveau temps caractéristique
					listeeq[k][2]=tcarac;
				}
			}
		
			// Il faut vérifier que la distribution est assez étendue pour prendre en compte toutes les réactions possibles
			if ((i+j)*2 > nbclasse)
			{
				// Numéro de la classe la plus grande précédemment remplie
				nbclassetemp=nbclasse/2;

				// Redimensionner le tableau de classes (en remplissant de 0)
				distrib.resize((i+j)*2,0);
				nbclasse=(i+j)*2;

				// Ajouter à la liste de réactions élémentaires
				for (i=nbclassetemp+1;i!=nbclasse/2+1;i++)
				{
					for (j=1;j!=i+1;j++)
					{
						listeeq.push_back(vector<double>(4,0));
						// Clusters agrégés dans cet équilibre
						listeeq[n][0]=j;
						listeeq[n][1]=i;
						// Temps caractéristique d'un cluster pour cet équilibre (une valeur négative signifie que la réaction est de type fragmentation) 
						listeeq[n][2]=1/((*noy).element(i,j,1)*distrib[i-1]*distrib[j-1]/volume-(*noy).element(i,j,2)*distrib[i+j-1]);
						// Temps restant avant avancement d'un cluster de l'équilibre (initialement égal au temps caractéristique, diminue avec l'écoulement du temps)
						listeeq[n][3]=abs(listeeq[n][2]);
						n++;
					}
				}
			}
		}


		// Tri des équilibres élémentaires par temps restant croissant
		sort(listeeq.begin(), listeeq.end(), triEq);

	}

	// Redimensionnement de la distribution (enlève les 0 inutiles en fin de distribution)
	i=nbclasse;
	while(distrib[i-1] == 0 )
	{
		i--;
	}
	distrib.resize(i);
	nbclasse=i;


	// Renvoie la durée effective de la simulation (premier évènement après la durée imposée)
	return suivi;
}

// Passage au pas de temps suivant (noyau, nombre de particules transférées)
double distributionquant::incremente (noyau * noy, int nb)
{
	// Variables de contrôle
	int i,j,k;
	// Nombre d'équilibres élémentaires (va augmenter quand la distribution s'étend)
	int n=0;
	// Sens de l'équilibre courant
	int ajout;
	// Pas de temps élémentaire jusqu'au prochain transfert de matière (variable)
	double deltat;
	// Temps caractéristique jusqu'au prochain transfert de matière (variable)
	double tcarac;
	// Nombre de classes temporaire
	int nbclassetemp;
	// Suivi du temps total
	double suivi=0;
	int suivinum=0;

	// Redimensionner le tableau de classes (en remplissant de 0)
	distrib.resize(nbclasse*2,0);
	nbclasse=nbclasse*2;

	// Préparer liste des états d'équilibres élémentaires (il sont avancés l'un après l'autre, par cinétique décroissante) 
	vector <vector <double>> listeeq;
	for (i=1;i!=nbclasse/2+1;i++)
	{
		for (j=1;j!=i+1;j++)
		{
			listeeq.push_back(vector<double>(4,0));
			// Clusters agrégés dans cet équilibre
			listeeq[n][0]=j;
			listeeq[n][1]=i;
			// Temps caractéristique d'un cluster pour cet équilibre (une valeur négative signifie que la réaction est de type fragmentation)
			// Cas particulier de la double agrégation (formule modifiée pour prendre en compte le cas de particules peu nombreuses)
			if (i==j)
			{
				listeeq[n][2]=1/((*noy).element(j,i,1)*distrib[i-1]*(distrib[j-1]-1)/volume-(*noy).element(j,i,2)*distrib[i+j-1]);
			}
			else
			{
				listeeq[n][2]=1/((*noy).element(j,i,1)*distrib[i-1]*distrib[j-1]/volume-(*noy).element(j,i,2)*distrib[i+j-1]);
			}
			// Temps restant avant avancement d'un cluster de l'équilibre (initialement égal au temps caractéristique, diminue avec l'écoulement du temps)
			listeeq[n][3]=abs(listeeq[n][2]);
			n++;

		}
	}

	// Tri des équilibres élémentaires par temps restant croissant
	sort(listeeq.begin(), listeeq.end(), triEq);

	// Tant que le temps imposé n'est pas atteint, ou que le temps caractéristique le plus court n'est pas infini (distribution stable), on applique les transferts de matière
	while (suivinum < nb && !isinf(listeeq[0][2]))
	{
		// Le premier équilibre de la liste est celui qui subit un avancement le premier

		// Sens de la réaction : positif (=1) si agrégation, négatif (-1) sinon
		ajout = (int) 1-2*signbit(listeeq[0][2]);
		i=(int) listeeq[0][1];
		j=(int) listeeq[0][0];
		deltat=listeeq[0][3];
		suivi+=listeeq[0][3];


		// On avance la réaction
		suivinum++;
		// Déplacement des clusters dans la distribution
		// Réactifs
		distrib[i-1] -= (double) ajout;
		distrib[j-1] -= (double) ajout;
		// Produit
		distrib[i+j-1] += (double) ajout;

		// Evolution des temps de réactions en cours
		listeeq[0][3]+=abs(listeeq[0][2]);

		for (k=0;k!=n;k++)
		{
			// Diminution des temps restants de toutes les réactions en cours
			listeeq[k][3]-=deltat;

			if (listeeq[k][0] == i || listeeq[k][0] == j || listeeq[k][1] == i || listeeq[k][1] == j || listeeq[k][0] == i+j || listeeq[k][1] == i+j || listeeq[k][0]+listeeq[k][1] == j || listeeq[k][0]+listeeq[k][1] == i || listeeq[k][0]+listeeq[k][1] == j+i)
			{
				if (i==j)
				{
					tcarac=1/((*noy).element(listeeq[k][1],listeeq[k][0],1)/volume*distrib[listeeq[k][0]-1]*(distrib[listeeq[k][1]-1]-1)-(*noy).element(listeeq[k][1],listeeq[k][0],2)*distrib[listeeq[k][0]+listeeq[k][1]-1]);
				}
				else
				{
					tcarac=1/((*noy).element(listeeq[k][1],listeeq[k][0],1)/volume*distrib[listeeq[k][0]-1]*distrib[listeeq[k][1]-1]-(*noy).element(listeeq[k][1],listeeq[k][0],2)*distrib[listeeq[k][0]+listeeq[k][1]-1]);
				}

				// Correction du temps restant
				if (isinf(listeeq[k][2]))
				{
					listeeq[k][3]=abs(tcarac);
				}
				else
				{
					listeeq[k][3]=(listeeq[k][3]/abs(listeeq[k][2]))*abs(tcarac);
				}

				// Nouveau temps caractéristique
				listeeq[k][2]=tcarac;
			}
		}
		
		// Il faut vérifier que la distribution est assez étendue pour prendre en compte toutes les réactions possibles
		if ((i+j)*2 > nbclasse)
		{
			// Numéro de la classe la plus grande précédemment remplie
			nbclassetemp=nbclasse/2;

			// Redimensionner le tableau de classes (en remplissant de 0)
			distrib.resize((i+j)*2,0);
			nbclasse=(i+j)*2;

			// Ajouter à la liste de réactions élémentaires
			for (i=nbclassetemp+1;i!=nbclasse/2+1;i++)
			{
				for (j=1;j!=i+1;j++)
				{
					listeeq.push_back(vector<double>(4,0));
					// Clusters agrégés dans cet équilibre
					listeeq[n][0]=j;
					listeeq[n][1]=i;
					// Temps caractéristique d'un cluster pour cet équilibre (une valeur négative signifie que la réaction est de type fragmentation) 
					// Cas particulier de la double agrégation (formule modifiée pour prendre en compte le cas de particules peu nombreuses)
					if (i==j)
					{
						listeeq[n][2]=1/((*noy).element(j,i,1)*distrib[i-1]*(distrib[j-1]-1)/volume-(*noy).element(j,i,2)*distrib[i+j-1]);
					}
					else
					{
						listeeq[n][2]=1/((*noy).element(j,i,1)*distrib[i-1]*distrib[j-1]/volume-(*noy).element(j,i,2)*distrib[i+j-1]);
					}
					// Temps restant avant avancement d'un cluster de l'équilibre (initialement égal au temps caractéristique, diminue avec l'écoulement du temps)
					listeeq[n][3]=abs(listeeq[n][2]);
					n++;
				}
			}
		}


		// Tri des équilibres élémentaires par temps restant croissant (pas nécessaire avec les modifications apportées à l'algorithme)
		sort(listeeq.begin(), listeeq.end(), triEq);

	}

	// Redimensionnement de la distribution (enlève les 0 inutiles en fin de distribution)
	i=nbclasse;
	while(distrib[i-1] == 0 )
	{
		i--;
	}
	distrib.resize(i);
	nbclasse=i;


	// Renvoie la durée effective de la simulation (premier évènement après la durée imposée)
	return suivi;
}

// Récupération des éléments de la distribution (classe i)
double distributionquant::element(int i) const
{
	// Si les indices sont dans la partie explicitement définie de la distribution, on les renvoie
	if (i<=nbclasse)
	{
		return distrib[i-1];
	}
	// Sinon cet élément est nul
	else
	{
		return 0;
	}
}

// Récupération de la taille de la distribution (va servir pour les opérations entre distributions)
int distributionquant::taille(void) const
{
		return nbclasse;
}

// Ajout d'une autre distribution à la distribution
void distributionquant::ajout(distributionquant * distr2)
{
	int i;

	// Redimensionne la distribution à la plus grande taille des deux ajoutées
	nbclasse = max(nbclasse,(*distr2).taille());
	distrib.resize(nbclasse,0);

	// Somme
	for (i=0;i!=nbclasse;i++)
	{
		distrib[i]=distrib[i]+(*distr2).element(i+1);
	}
}
	
// Distance à une autre distribution (avec ajustement pour la concentration en particules), type = TVD (Total Variation Distance) - Hellinger - Bhattacharyya  
double distributionquant::distance(distributionquant * distr2, std::string type) const
{
	// Taille des distributions
	int taille;
	// Distance
	double distance=0;
	int i;
	// Ajustements pour la concentration totale en particules élémentaires (doit passer les intégrales des deux distributions à 1 -> même nombre de particules)
	double ajust1= 1/populationval();
	double ajust2= 1/(*distr2).populationval();

	// Redimensionne la distribution à la plus grande taille des deux ajoutées
	taille = max(nbclasse,(*distr2).taille());
		
	// Distance TVD (plus grand écart entre deux classes) 
	if (type == "TVD")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			if (abs(distrib[i]-(*distr2).element(i+1)) > distance)
			{
				distance = abs((i+1)*distrib[i]*ajust1-(i+1)*(*distr2).element(i+1)*ajust2);
			}
		}
	}
		
	// Distance Hellinger (sensible aux erreurs d'arrondi)
	if (type == "Hellinger")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			distance = distance + pow(pow((i+1)*distrib[i]*ajust1,0.5)-pow((i+1)*(*distr2).element(i+1)*ajust2,0.5),2);
		}
		distance=pow(distance/2,0.5);
	}
		
	// Distance Bhattacharyya (on renormalise les distributions à 1 pour avoir une distance nulle en cas d'identité)
	if (type == "Bhattacharyya")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			distance = distance + (i+1)*pow(distrib[i]*(*distr2).element(i+1)*ajust1*ajust2,0.5);
		}
		distance=abs(-log(distance));
	}
		
	return distance;
}

// Calcul des flux de matière pour chaque équilibre élémentaire, avec un pas de temps de 1
void distributionquant::flux (noyau * noy) const
{
	int i,j;
	// Agglomération et dislocation
	for (i=0;i!=nbclasse;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			// Ecriture à l'écran
			cout << "Phi" << j+1 << "-" << i+1 << "=" << (*noy).element(i+1,j+1,1)*this->element(i+1)*this->element(j+1)/(volume*volume)-(*noy).element(i+1,j+1,2)*this->element(i+j+2)/(volume) << endl;
		}
	}	
}
// Ecriture dans un fichier
void distributionquant::flux (noyau * noy, std::string nom) const
{
	int i,j;
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
	{    
		for (i=0;i!=nbclasse;i++)
		{
			for (j=0;j!=i+1;j++)
			{
				fichier << "Phi" << j+1 << "-" << i+1 << "=" << (*noy).element(i+1,j+1,1)*this->element(i+1)*this->element(j+1)/(volume*volume)-(*noy).element(i+1,j+1,2)*this->element(i+j+2)/(volume) << endl;
			}
		}
		fichier << endl;
           	fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}

// Multiplication de tous les termes de la distribution par un même facteur
void distributionquant::dilution (float facteur)
{
	int i;

	// Somme
	for (i=0;i!=nbclasse;i++)
	{
		distrib[i]=distrib[i]*facteur;
	}
}

// Calcul des constantes d'équilibre normalisées pour chaque réaction élémentaire
// Note : les constantes peuvent parfois prendre des valeurs non réelles - infini par exemple
void distributionquant::equilibres (noyau * noy) const
{
	int i,j;
	for (i=2;i!=nbclasse+1;i++)
	{
		for (j=1;j!=floor(i/2)+1;j++)
		{
			// Ecriture à l'écran
			cout << "K" << j << "-" << i-j << "=" << (*noy).element(j,i-j,2)/(*noy).element(j,i-j,1)*distrib[i-1]*(volume)/(distrib[i-j-1]*distrib[j-1]) << endl;
		}
	}	
}
// Ecriture dans un fichier
void distributionquant::equilibres (noyau * noy, std::string nom) const
{
	int i,j;
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
	{    
		for (i=2;i!=nbclasse+1;i++)
		{
			for (j=1;j!=floor(i/2)+1;j++)
			{
				// Ecriture à l'écran
				fichier << "K" << j << "-" << i-j << "=" << (*noy).element(j,i-j,2)/(*noy).element(j,i-j,1)*distrib[i-1]*(volume)/(distrib[i-j-1]*distrib[j-1]) << endl;
			}
		}
		fichier << endl;
           	fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier " << nom << " !" << endl;
	}
}

// Calcul d'un critère d'évolution (ou d'équilibre ?) du bilan de population
double distributionquant::critere (noyau * noy) const
{
	// Préparer accumulateur de la bonne taille (initialisé à 0)
	vector<double> vardistrib (nbclasse*2,0);

	// Doubler variables de contrôle
	int i,j;
	double resultat;
	double critere=0;
		

			// Agglomération et dislocation

			for (i=0;i!=nbclasse;i++)
			{
				for (j=0;j!=i+1;j++)
				{
					// Agglomeration
					resultat=(*noy).element(i+1,j+1,1)*distrib[i]*distrib[j]/(volume*volume);
					if (distrib[i]<0 && distrib[j]<0)
					{
						resultat=-resultat;
					}
					vardistrib[i] = vardistrib[i] -resultat;				
					vardistrib[j] = vardistrib[j] -resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] +resultat;
	
					// Dislocation
					resultat=(*noy).element(i+1,j+1,2)*distrib[i+j+1]/(volume);
					vardistrib[i] = vardistrib[i] +resultat;				
					vardistrib[j] = vardistrib[j] +resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] -resultat;
				}
			}	
								
	// Mise à jour de la distribution (on se limite au maximum en termes d'extension)
	i=nbclasse*2-1;
		
	// repérage de la classe non vide la plus élevée de l'accumulateur (limite le nombre d'opérations - note, on perd la conservation rigoureuse des particules, car les valeurs inf à 10-324 sont considérées nulles)
	while (vardistrib[i] == 0 && i!=-1)
	{
		i--;
	}

	// Calcul du critere proprement dit
	for (;i!=-1;i--)
	{
		critere=critere+vardistrib[i]*vardistrib[i];
	}
	return critere;	
}

// Evolution de la distribution lors d'un avancement forcé de l'une des réactions élémentaires 
void distributionquant::avancement (int i, int j, int av)
{
	distrib[i-1]=distrib[i-1]-av;
	distrib[j-1]=distrib[j-1]-av;
	distrib[i+j-1]=distrib[i+j-1]+av;	
}

// Condition limite de type Neumann (impose une concentration dans une classe)
double distributionquant::neumann (int i, double val)
{
	int j=i;
	double flux;
	// Vérification de l'existence des classes demandées
	// Si numéro négatif, on affecte à la classe 1 
	if (i<0)
	{
		j=1;
	}
	// Si numéro trop élevé, on affecte à la classe la plus grande
	if (i>nbclasse)
	{
		j=nbclasse;
	}
	flux=val-distrib[j-1];
	distrib[j-1]=val;
	return flux;
}

// Calcul d'un critère de compatibilité entre un noyau et la distribution vue comme un état d'équilibre
double distributionquant::critcompat (noyau * noy) const
{
	int i,j;

	// Stockage de chaque valeur correspondant à une vitesse d'évolution de concentration (le critère correspond à la somme de leurs carrés)
	double result[nbclasse];
	for (i=0;i!=nbclasse;i++)
	{
		result[i]=0;
	}
	double resultatfinal=0;

	// Calcul des flux élémentaires et stockage (le flux est enlevé à chaque classe de réactif et ajouté à chaque classe de produit)
	for (i=2;i!=nbclasse+1;i++)
	{
		for (j=1;j!=(int) floor(i/2)+1;j++)
		{
			// Ecriture à l'écran
			resultatfinal=(*noy).element(i-j,j,1)*distrib[j-1]*distrib[i-j-1]/(volume*volume)-(*noy).element(i-j,j,2)*distrib[i-1]/volume;
			result[i-j-1]=result[i-j-1]-resultatfinal;
			result[j-1]=result[j-1]-resultatfinal;
			result[i-1]=result[i-1]+resultatfinal;
		}
	}

	// Calcul du critère proprement dit
	resultatfinal=0;
	for (i=0;i!=nbclasse;i++)
	{
		resultatfinal=resultatfinal+result[i]*result[i];
	}

	return resultatfinal;		
}
