#include "Distribution.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <cmath>

using namespace std;

// Objet : distribution de tailles de particules
// Méthodes : 	distribution - construction
//				ecrire - affichage ou sauvegarde de la distribution
//				population - calcul du total de particules élémentaires (vérifie la conservation)
//				incremente - calcul du pas de temps suivant à l'aide d'un noyau d'aggrégation

	
// Nombre de classes
//int nbclasse;
// Tableau de la distribution (à partir de 1 particule)
//vector<double> distrib;


// Constructeur de base non nécessaire
	
// Constructeur à partir d'un nom de fichier
distribution::distribution (std::string nom)
{
	nbclasse=0;
	// Génère la distribution initiale à partir d'un fichier - classe de particule = numéro de ligne (en partant de la particule élémentaire) / concentration
	ifstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
     	{    
		string ligne;
		while (getline(fichier,ligne))
		{
			nbclasse++;
			distrib.push_back(atof(ligne.c_str()));
		}	
       		fichier.close();
	}
	else
	{
        cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}

// Constructeur de copie
distribution::distribution (const distribution &dis)
{
	int i;
	nbclasse=dis.nbclasse;
	// Copie terme à terme de la distribution
	for (i=0;i!=nbclasse;i++)
	{
		distrib.push_back(dis.distrib[i]);
	}
}

// Destructeur spécifique non nécessaire

// Fonction d'écriture - classe de particule / concentration
// Ecriture à l'écran
void distribution::ecrire (void) const
{
	int i;
	cout << endl << "Taille (particules elementaires)\tConcentration" << endl;
	for (i=0;i!=nbclasse;i++)
	{
		cout << i+1 << "\t\t\t\t\t" << distrib[i] << endl;
	}
	cout << endl;
}
// Ecriture dans un fichier
void distribution::ecrire (std::string nom) const
{
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
	{    
		int i;
		for (i=0;i!=nbclasse;i++)
		{
			fichier << distrib[i] << " ";
		}
		fichier << endl;
           fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}
// Variante qui enregistre le pas de temps
void distribution::ecrire (std::string nom, double temps) const
{
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
     	{    
		int i;
		fichier << temps << " ";
		for (i=0;i!=nbclasse;i++)
		{
			fichier << distrib[i] << " ";
		}
		fichier << endl;
           fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}

// Ecrit la valeur de population totale (concentration totale en particules élémentaires)
void distribution::population (void) const
{
	int i;
	double somme=0;
	for (i=0;i!=nbclasse;i++)
	{
		somme=somme+distrib[i]*(i+1);
	}
	cout << somme << endl;
}
// Version à usage interne (renvoie la valeur au lieu de l'écrire)
double distribution::populationval (void) const
{
	int i;
	double somme=0;
	for (i=0;i!=nbclasse;i++)
	{
		somme=somme+distrib[i]*(i+1);
	}
	return somme;
}

// Version à usage interne (renvoie la valeur de l'intégrale de la distribution)
double distribution::integrale (void) const
{
	int i;
	double somme=0;
	for (i=0;i!=nbclasse;i++)
	{
		somme=somme+distrib[i];
	}
	return somme;
}

// Passage au pas de temps suivant (noyau, longueur du pas)
double distribution::incremente (noyau * noy, double pastps)
{
	// Préparer accumulateur de la bonne taille (initialisé à 0)
	vector<double> vardistrib (nbclasse*2,0);

	// Doubler variables de contrôle
	int i,j;
	double resultat;
	double mult;
		
	// Redimensionner le tableau de classes (en remplissant de 0)
	distrib.resize(nbclasse*2,0);

			// Agglomération et dislocation

			for (i=0;i!=nbclasse;i++)
			{
				for (j=0;j!=i+1;j++)
				{
					// Agglomeration
					resultat=(*noy).element(i+1,j+1,1)*distrib[i]*distrib[j];
					vardistrib[i] = vardistrib[i] -resultat;				
					vardistrib[j] = vardistrib[j] -resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] +resultat;
	
					// Dislocation
					resultat=(*noy).element(i+1,j+1,2)*distrib[i+j+1];
					vardistrib[i] = vardistrib[i] +resultat;				
					vardistrib[j] = vardistrib[j] +resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] -resultat;
				}
			}	
								
	// Mise à jour de la distribution (on se limite au maximum en termes d'extension)
	i=nbclasse*2-1;
		
	// repérage de la classe non vide la plus élevée de l'accumulateur (limite le nombre d'opérations - note, on perd la conservation rigoureuse des particules, car les valeurs inf à 10-324 sont considérées nulles)
	while (vardistrib[i] == 0 && i!=-1)
	{
		i--;
	}

	// Redimensionnement de la distribution, avec classes supplémentaires à 0 (juste ce qu'il faut)
	nbclasse=max(nbclasse,i+1);
	distrib.resize(nbclasse,0);

	// Ajout de l'accumulateur à la distribution
	mult=pastps;
	
	// Vérification du non dépassement (les concentrations obtenues ne doivent pas être négatives) : on détermine le pas de temps maximal pour éviter cet effet
	for (j=i;j!=-1;j--)
	{
		// Ne peut se produire que si l'incrément de concentration est négatif
		if (distrib[j]+vardistrib[j]*mult < 0)
		{
			// Légère modification de la formule pour prendre en compte les erreurs liées aux opérations sur les double
			mult=-(0.999999999999*distrib[j])/vardistrib[j];
		}
	}
	
	for (;i!=-1;i--)
	{
		distrib[i]=distrib[i]+vardistrib[i]*mult;
	}
	return mult;
}

// Passage au pas de temps suivant (noyau, longueur du pas) - Runge Kutta ordre 2
double distribution::incrementeRK2 (noyau * noy, double pastps)
{
	// Préparer accumulateurs de la bonne taille (initialisé à 0)
	vector<double> vardistrib (nbclasse*2,0);

	// Stockage distribution à mi-parcours (initialisé à 0)
	vector<double> distrib2 (nbclasse*2,0);

	// Doubler variables de contrôle
	int i,j;
	double resultat;
	double mult;
		
	// Redimensionner le tableau de classes (en remplissant de 0)
	distrib.resize(nbclasse*2,0);

			// Agglomération et dislocation

			for (i=0;i!=nbclasse;i++)
			{
				for (j=0;j!=i+1;j++)
				{
					// Agglomeration
					resultat=(*noy).element(i+1,j+1,1)*distrib[i]*distrib[j];
					vardistrib[i] = vardistrib[i] -resultat;				
					vardistrib[j] = vardistrib[j] -resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] +resultat;
	
					// Dislocation
					resultat=(*noy).element(i+1,j+1,2)*distrib[i+j+1];
					vardistrib[i] = vardistrib[i] +resultat;				
					vardistrib[j] = vardistrib[j] +resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] -resultat;
				}
			}	
								
	// Mise à jour de la distribution (on se limite au maximum en termes d'extension)
	i=nbclasse*2-1;
		
	// repérage de la classe non vide la plus élevée de l'accumulateur (limite le nombre d'opérations - note, on perd la conservation rigoureuse des particules, car les valeurs inf à 10-324 sont considérées nulles)
	while (vardistrib[i] == 0 && i!=-1)
	{
		i--;
	}

	// Redimensionnement de la distribution, avec classes supplémentaires à 0 (juste ce qu'il faut)
	nbclasse=max(nbclasse,i+1);
	distrib2.resize(nbclasse,0);

	// Ajout de l'accumulateur à la distribution
	mult=pastps/2;
	
	// Vérification du non dépassement (les concentrations obtenues ne doivent pas être négatives) : on détermine le pas de temps maximal pour éviter cet effet
	for (j=i;j!=-1;j--)
	{
		// Ne peut se produire que si l'incrément de concentration est négatif
		if (distrib[j]+vardistrib[j]*mult < 0)
		{
			// Légère modification de la formule pour prendre en compte les erreurs liées aux opérations sur les double
			mult=-(0.999999999999*distrib[j])/vardistrib[j];
		}
	}
	
	// Calcul de la distribution estimée à mi-parcours du pas de temps + nettoyage de l'accumulateur pour réutilisation
	for (;i!=-1;i--)
	{
		distrib2[i]=distrib[i]+vardistrib[i]*mult;
		vardistrib[i]=0;
	}

	// Redimensionner le tableau de classes (en remplissant de 0) + accumulateur
	distrib.resize(nbclasse*2,0);
	vardistrib.resize(nbclasse*2,0);


			// Agglomération et dislocation à mi-parcours
			for (i=0;i!=nbclasse;i++)
			{
				for (j=0;j!=i+1;j++)
				{
					// Agglomeration
					resultat=(*noy).element(i+1,j+1,1)*distrib2[i]*distrib2[j];
					vardistrib[i] = vardistrib[i] -resultat;				
					vardistrib[j] = vardistrib[j] -resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] +resultat;
	
					// Dislocation
					resultat=(*noy).element(i+1,j+1,2)*distrib2[i+j+1];
					vardistrib[i] = vardistrib[i] +resultat;				
					vardistrib[j] = vardistrib[j] +resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] -resultat;
				}
			}	

	// Mise à jour de la distribution (on se limite au maximum en termes d'extension)
	i=nbclasse*2-1;
		
	// repérage de la classe non vide la plus élevée de l'accumulateur (limite le nombre d'opérations - note, on perd la conservation rigoureuse des particules, car les valeurs inf à 10-324 sont considérées nulles)
	while (vardistrib[i] == 0 && i!=-1)
	{
		i--;
	}

	// Redimensionnement de la distribution, avec classes supplémentaires à 0 (juste ce qu'il faut)
	nbclasse=max(nbclasse,i+1);
	distrib.resize(nbclasse,0);

	// Ajout de l'accumulateur à la distribution
	mult=mult*2;
	
	// Vérification du non dépassement (les concentrations obtenues ne doivent pas être négatives) : on détermine le pas de temps maximal pour éviter cet effet
	for (j=i;j!=-1;j--)
	{
		// Ne peut se produire que si l'incrément de concentration est négatif
		if (distrib[j]+vardistrib[j]*mult < 0)
		{
			// Légère modification de la formule pour prendre en compte les erreurs liées aux opérations sur les double
			mult=-(0.999999999999*distrib[j])/vardistrib[j];
		}
	}

	for (;i!=-1;i--)
	{
		distrib[i]=distrib[i]+vardistrib[i]*mult;
	}
	return mult;
}

// Passage au pas de temps suivant - méthode avancée pour les cas de forte hétérogénéité du noyau (noyau, longueur du pas)
double distribution::incrementeava (noyau * noy, double pastps)
{
	// Préparer 2 accumulateurs de la bonne taille pour les variations de distribution (initialisés à 0), l'un ne contient que les incréments de concentration, l'autre que les soustractions
	vector<double> vardistribplus (nbclasse*2,0);
	vector<double> vardistribmoins (nbclasse*2,0);

	// Doubler variables de contrôle
	int i,j,k,l;
	int nombreannulations;
	double resultat;
	double facteur;

	// Stockage (a minima) des flux de matière instantanés
	std::vector<double> fluxdisl;
	std::vector<double> fluxaggr;

	// Redimensionner le tableau de classes (en remplissant de 0)
	distrib.resize(nbclasse*2,0);

	// Agglomération et dislocation : stockage des vitesses instantanées de flux de matière + somme dans l'accumulateur de la distribution

	for (i=0;i!=nbclasse;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			// Agglomeration
			resultat=(*noy).element(i+1,j+1,1)*distrib[i]*distrib[j];
			fluxaggr.push_back(resultat);
			vardistribmoins[i] = vardistribmoins[i] -resultat;				
			vardistribmoins[j] = vardistribmoins[j] -resultat;
			vardistribplus[i+j+1] = vardistribplus[i+j+1] +resultat;

			// Dislocation
			resultat=(*noy).element(i+1,j+1,2)*distrib[i+j+1];
			fluxdisl.push_back(resultat);
			vardistribplus[i] = vardistribplus[i] +resultat;				
			vardistribplus[j] = vardistribplus[j] +resultat;
			vardistribmoins[i+j+1] = vardistribmoins[i+j+1] -resultat;
		}
	}	

	// Test d'annulation d'une classe (on boucle sur toutes les classes successivement autant de fois que nécessaire)
	// Première boucle : totalité de la distribution
	for (nombreannulations=1;nombreannulations!=0;)
	{
		// Remise à 0 du nombre d'annulations 
		nombreannulations=0;

		// Boucle sur éléments de la distribution
		for (k=0;k!=nbclasse*2;k++)
		{
			// Si la classe atteint une concentration négative durant le pas de temps
			if (distrib[k]+(vardistribplus[k]+vardistribmoins[k])*pastps < 0)
			{

				nombreannulations++;

				// Calcul de la fraction du pas de temps durant laquelle les flux soustraits doivent être désactivés : tous les flux soustractifs liés à cette classe de la distribution doivent être proportionnés par (1-facteur)
				facteur = (distrib[k]+(vardistribplus[k]+vardistribmoins[k])*pastps)/(vardistribmoins[k]*pastps);

				// Recalcul des flux, puis des variations de distributions (méthode bourrine)
				l=-1;
				for (i=0;i!=nbclasse;i++)
				{
					for (j=0;j!=i+1;j++)
					{
						l++;
						// Si le flux considéré fait partie des flux affectés par la classe k
						if (i==k || j==k)
						{
							// Agglomeration
							vardistribplus[i+j+1] = vardistribplus[i+j+1] -fluxaggr[l]*facteur;
							vardistribmoins[i] = vardistribmoins[i] +fluxaggr[l]*facteur;				
							vardistribmoins[j] = vardistribmoins[j] +fluxaggr[l]*facteur;
							fluxaggr[l]=fluxaggr[l]*(1-facteur);
						}
						
						if (i+j+1==k)
						{
							// Dislocation
							vardistribmoins[i+j+1] = vardistribmoins[i+j+1] +fluxdisl[l]*facteur;
							vardistribplus[i] = vardistribplus[i] -fluxdisl[l]*facteur;				
							vardistribplus[j] = vardistribplus[j] -fluxdisl[l]*facteur;
							fluxdisl[l]=fluxdisl[l]*(1-facteur);
						}					
					}
					
				}
			}
		}
	}
							
	// Mise à jour de la distribution (on se limite au maximum en termes d'extension)
	i=nbclasse*2-1;
		
	// repérage de la classe non vide la plus élevée de l'accumulateur (limite le nombre d'opérations - note, on perd la conservation rigoureuse des particules, car les valeurs inf à 10-324 sont considérées nulles)
	while (vardistribmoins[i] + vardistribplus[i] == 0 && i!=-1)
	{
		i--;
	}

	// Redimensionnement de la distribution, avec classes supplémentaires à 0 (juste ce qu'il faut)
	nbclasse=max(nbclasse,i+1);
	distrib.resize(nbclasse,0);

	// Ajout de l'accumulateur à la distribution	
	for (;i!=-1;i--)
	{
		distrib[i]=distrib[i]+(vardistribmoins[i]+vardistribplus[i])*pastps;
	}
	return pastps;
}

// Passage au pas de temps suivant - méthode pour inversion du bilan de populations, nécessite l'équivalent d'un pas de temps avancé comme pour les cas de forte hétérogénéité du noyau (noyau, longueur du pas)
double distribution::incrementeinv (noyau * noy, double pastps)
{
	// Préparer 2 accumulateurs de la bonne taille pour les variations de distribution (initialisés à 0), l'un ne contient que les incréments de concentration, l'autre que les soustractions
	vector<double> vardistribplus (nbclasse*2,0);
	vector<double> vardistribmoins (nbclasse*2,0);

	// Doubler variables de contrôle
	int i,j,k,l;
	int nombreannulations;
	double resultat;
	double facteur;

	// Stockage (a minima) des flux de matière instantanés
	std::vector<double> fluxdisl;
	std::vector<double> fluxaggr;

	// Redimensionner le tableau de classes (en remplissant de 0)
	distrib.resize(nbclasse*2,0);

	// Agglomération et dislocation : stockage des vitesses instantanées de flux de matière + somme dans l'accumulateur de la distribution

	for (i=0;i!=nbclasse;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			// Agglomeration
			resultat=(*noy).element(i+1,j+1,1)*distrib[i]*distrib[j];
			fluxaggr.push_back(resultat);
			vardistribmoins[i] = vardistribmoins[i] +resultat;				
			vardistribmoins[j] = vardistribmoins[j] +resultat;
			vardistribplus[i+j+1] = vardistribplus[i+j+1] -resultat;

			// Dislocation
			resultat=(*noy).element(i+1,j+1,2)*distrib[i+j+1];
			fluxdisl.push_back(resultat);
			vardistribplus[i] = vardistribplus[i] -resultat;				
			vardistribplus[j] = vardistribplus[j] -resultat;
			vardistribmoins[i+j+1] = vardistribmoins[i+j+1] +resultat;
		}
	}	

	// Test d'annulation d'une classe (on boucle sur toutes les classes successivement autant de fois que nécessaire)
	// Première boucle : totalité de la distribution
int m=0;
	for (nombreannulations=1;nombreannulations!=0;)
	{
		// Remise à 0 du nombre d'annulations 
		nombreannulations=0;

		// Boucle sur éléments de la distribution
		for (k=0;k!=nbclasse*2;k++)
		{
			// Si la classe atteint une concentration négative durant le pas de temps
			if (distrib[k]+(vardistribplus[k]+vardistribmoins[k])*pastps < 0)
			{

				nombreannulations++;

				// Calcul de la fraction du pas de temps durant laquelle les flux soustraits doivent être désactivés : tous les flux soustractifs liés à cette classe de la distribution doivent être proportionnés par (1-facteur)
				facteur = (distrib[k]+(vardistribplus[k]+vardistribmoins[k])*pastps)/(vardistribplus[k]*pastps);

				// Recalcul des flux, puis des variations de distributions (méthode bourrine)
				l=-1;
				for (i=0;i!=nbclasse;i++)
				{
					for (j=0;j!=i+1;j++)
					{
						l++;
						// Si le flux considéré fait partie des flux affectés par la classe k
						if (i==k || j==k)
						{
							// Agglomeration
							vardistribplus[i+j+1] = vardistribplus[i+j+1] +fluxaggr[l]*facteur;
							vardistribmoins[i] = vardistribmoins[i] -fluxaggr[l]*facteur;				
							vardistribmoins[j] = vardistribmoins[j] -fluxaggr[l]*facteur;
							fluxaggr[l]=fluxaggr[l]*(1-facteur);
						}
						
						if (i+j+1==k)
						{
							// Dislocation
							vardistribmoins[i+j+1] = vardistribmoins[i+j+1] -fluxdisl[l]*facteur;
							vardistribplus[i] = vardistribplus[i] +fluxdisl[l]*facteur;				
							vardistribplus[j] = vardistribplus[j] +fluxdisl[l]*facteur;
							fluxdisl[l]=fluxdisl[l]*(1-facteur);
						}					
					}
					
				}
			}
		}
m++;
// Si plus de 2 boucles sans résolution, on considère que l'on atteint une frontière avec vitesse non parallèle 
if (m>2)
{
nombreannulations=0;
}
	}
							
	// Mise à jour de la distribution (on se limite au maximum en termes d'extension)
	i=nbclasse*2-1;
		
	// repérage de la classe non vide la plus élevée de l'accumulateur (limite le nombre d'opérations - note, on perd la conservation rigoureuse des particules, car les valeurs inf à 10-324 sont considérées nulles)
	while (vardistribmoins[i] + vardistribplus[i] == 0 && i!=-1)
	{
		i--;
	}

	// Redimensionnement de la distribution, avec classes supplémentaires à 0 (juste ce qu'il faut)
	nbclasse=max(nbclasse,i+1);
	distrib.resize(nbclasse,0);

	// Ajout de l'accumulateur à la distribution	
	for (;i!=-1;i--)
	{
		distrib[i]=distrib[i]+(vardistribmoins[i]+vardistribplus[i])*pastps;
	}

// Si le calcul est interrompu par la frontière, on renvoie un pas de temps infini
if (m>2)
{
pastps=pastps/0.0;
}
	return pastps;
}

// Récupération des éléments de la distribution (classe i)
double distribution::element(int i) const
{
	// Si les indices sont dans la partie explicitement définie de la distribution, on les renvoie
	if (i<=nbclasse)
	{
		return distrib[i-1];
	}
	// Sinon cet élément est nul
	else
	{
		return 0;
	}
}

// Récupération de la taille de la distribution (va servir pour les opérations entre distributions)
int distribution::taille(void) const
{
		return nbclasse;
}

// Ajout d'une autre distribution à la distribution
void distribution::ajout(distribution * distr2)
{
	int i;

	// Redimensionne la distribution à la plus grande taille des deux ajoutées
	nbclasse = max(nbclasse,(*distr2).taille());
	distrib.resize(nbclasse,0);

	// Somme
	for (i=0;i!=nbclasse;i++)
	{
		distrib[i]=distrib[i]+(*distr2).element(i+1);
	}
}
	
// Distance à une autre distribution (avec ajustement pour la concentration en particules), type = TVD (Total Variation Distance) - Hellinger - Bhattacharyya  
double distribution::distance(distribution * distr2, std::string type) const
{
	// Taille des distributions
	int taille;
	// Distance
	double distance=0;
	int i;
	// Ajustements pour la concentration totale en particules élémentaires (doit passer les intégrales des deux distributions à 1 -> même nombre de particules)
	//double ajust1= 1/populationval();
	//double ajust2= 1/(*distr2).populationval();
	double ajust1= 1/integrale();
	double ajust2= 1/(*distr2).integrale();

	// Redimensionne la distribution à la plus grande taille des deux ajoutées
	taille = max(nbclasse,(*distr2).taille());
		
	// Distance TVD (plus grand écart entre deux classes) 
	if (type == "TVD")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			if (abs(distrib[i]-(*distr2).element(i+1)) > distance)
			{
				//distance = abs((i+1)*distrib[i]*ajust1-(i+1)*(*distr2).element(i+1)*ajust2);
				distance = abs(distrib[i]-(*distr2).element(i+1));
			}
		}
	}
		
	// Distance Hellinger (sensible aux erreurs d'arrondi)
	if (type == "Hellinger")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			//distance = distance + pow(pow((i+1)*distrib[i]*ajust1,0.5)-pow((i+1)*(*distr2).element(i+1)*ajust2,0.5),2);
			distance = distance + pow(pow(distrib[i]*ajust1,0.5)-pow((*distr2).element(i+1)*ajust2,0.5),2);
		}
		distance=pow(distance/2,0.5);
	}
		
	// Distance Bhattacharyya (on renormalise les distributions à 1 pour avoir une distance nulle en cas d'identité)
	if (type == "Bhattacharyya")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			//distance = distance + (i+1)*pow(distrib[i]*(*distr2).element(i+1)*ajust1*ajust2,0.5);
			distance = distance + pow(distrib[i]*(*distr2).element(i+1)*ajust1*ajust2,0.5);
		}
		distance=abs(-log(distance));
	}

	// Distance Manhattan
	if (type == "Manhattan")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			distance = distance + abs(distrib[i]-(*distr2).element(i+1));
		}
	}

	// Distance Mahalanobis
	if (type == "Mahalanobis")
	{
		// Calcul proprement dit
		for (i=0;i!=taille;i++)
		{
			distance = distance + pow(distrib[i]-(*distr2).element(i+1),2)/pow((*distr2).element(i+1)*0.05,2);
		}
	}
	distance = pow(distance,0.5);

	return distance;
}

// Calcul des flux de matière pour chaque équilibre élémentaire, avec un pas de temps de 1
void distribution::flux (noyau * noy) const
{
	int i,j;
	// Agglomération et dislocation
	for (i=0;i!=nbclasse;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			// Ecriture à l'écran
			cout << "Phi" << j+1 << "-" << i+1 << "=" << (*noy).element(i+1,j+1,1)*distrib[i]*distrib[j]-(*noy).element(i+1,j+1,2)*distrib[i+j+1] << endl;
		}
	}	
}
// Ecriture dans un fichier
void distribution::flux (noyau * noy, std::string nom) const
{
	int i,j;
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
	{    
		for (i=0;i!=nbclasse;i++)
		{
			for (j=0;j!=i+1;j++)
			{
				fichier << "Phi" << j+1 << "-" << i+1 << "=" << (*noy).element(i+1,j+1,1)*distrib[i]*distrib[j]-(*noy).element(i+1,j+1,2)*distrib[i+j+1] << " ";
			}
		}
		fichier << endl;
           	fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}

// Multiplication de tous les termes de la distribution par un même facteur
void distribution::dilution (float facteur)
{
	int i;

	// Somme
	for (i=0;i!=nbclasse;i++)
	{
		distrib[i]=distrib[i]*facteur;
	}
}

// Calcul des constantes d'équilibre normalisées pour chaque réaction élémentaire
void distribution::equilibres (noyau * noy) const
{
	int i,j;
	for (i=2;i!=nbclasse+1;i++)
	{
		for (j=1;j!=floor(i/2)+1;j++)
		{
			// Ecriture à l'écran
			cout << "K" << j << "-" << i-j << "=" << (*noy).element(j,i-j,2)/(*noy).element(j,i-j,1)*distrib[i-1]/(distrib[i-j-1]*distrib[j-1]) << endl;
		}
	}	
}
// Ecriture dans un fichier
void distribution::equilibres (noyau * noy, std::string nom) const
{
	int i,j;
	// Ajout à la suite du fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::app); 
	if(fichier.is_open())
	{    
		for (i=2;i!=nbclasse+1;i++)
		{
			for (j=1;j!=floor(i/2)+1;j++)
			{
				// Ecriture à l'écran
				fichier << "K" << j << "-" << i-j << "=" << (*noy).element(j,i-j,2)/(*noy).element(j,i-j,1)*distrib[i-1]/(distrib[i-j-1]*distrib[j-1]) << endl;
			}
		}
		fichier << endl;
           	fichier.close();
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier " << nom << " !" << endl;
	}
}

// Calcul d'un critère d'évolution (ou d'équilibre ?) du bilan de population
double distribution::critere (noyau * noy) const
{
	// Préparer accumulateur de la bonne taille (initialisé à 0)
	vector<double> vardistrib (nbclasse*2,0);

	// Doubler variables de contrôle
	int i,j;
	double resultat;
	double critere=0;
		

			// Agglomération et dislocation

			for (i=0;i!=nbclasse;i++)
			{
				for (j=0;j!=i+1;j++)
				{
					// Agglomeration
					resultat=(*noy).element(i+1,j+1,1)*distrib[i]*distrib[j];
					if (distrib[i]<0 && distrib[j]<0)
					{
						resultat=-resultat;
					}
					vardistrib[i] = vardistrib[i] -resultat;				
					vardistrib[j] = vardistrib[j] -resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] +resultat;
	
					// Dislocation
					resultat=(*noy).element(i+1,j+1,2)*distrib[i+j+1];
					vardistrib[i] = vardistrib[i] +resultat;				
					vardistrib[j] = vardistrib[j] +resultat;
					vardistrib[i+j+1] = vardistrib[i+j+1] -resultat;
				}
			}	
								
	// Mise à jour de la distribution (on se limite au maximum en termes d'extension)
	i=nbclasse*2-1;
		
	// repérage de la classe non vide la plus élevée de l'accumulateur (limite le nombre d'opérations - note, on perd la conservation rigoureuse des particules, car les valeurs inf à 10-324 sont considérées nulles)
	while (vardistrib[i] == 0 && i!=-1)
	{
		i--;
	}

	// Calcul du critere proprement dit
	for (;i!=-1;i--)
	{
		critere=critere+vardistrib[i]*vardistrib[i];
	}
	return critere;	
}

// Evolution de la distribution lors d'un avancement forcé de l'une des réactions élémentaires 
void distribution::avancement (int i, int j, double av)
{
	distrib[i-1]=distrib[i-1]-av;
	distrib[j-1]=distrib[j-1]-av;
	distrib[i+j-1]=distrib[i+j-1]+av;	
}

// Condition limite de type Neumann (impose une concentration dans une classe)
double distribution::neumann (int i, double val)
{
	int j=i;
	double flux;
	// Vérification de l'existence des classes demandées
	// Si numéro négatif, on affecte à la classe 1 
	if (i<0)
	{
		j=1;
	}
	// Si numéro trop élevé, on affecte à la classe la plus grande
	if (i>nbclasse)
	{
		j=nbclasse;
	}
	flux=val-distrib[j-1];
	distrib[j-1]=val;
	return flux;
}

// Condition limite de type Dirichlet (impose une variation concentration dans une classe - injection ou extraction de clusters)
double distribution::dirichlet (int i, double var, double pastps)
{
	int j=i;
	double diff;
	// Vérification de l'existence des classes demandées
	// Si numéro négatif, on affecte à la classe 1 
	if (i<0)
	{
		j=1;
	}
	// Si numéro trop élevé, on affecte à la classe la plus grande
	if (i>nbclasse)
	{
		j=nbclasse;
	}
	diff=distrib[j-1]-var*pastps;
	if (diff>=0)
	{
		distrib[j-1]=diff;
		return pastps;
	}
	else
	{
		distrib[j-1]=0;
		return pastps+diff/var;
	}
}

// Calcul d'un critère de compatibilité entre un noyau et la distribution vue comme un état d'équilibre
double distribution::critcompat (noyau * noy) const
{
	int i,j;

	// Stockage de chaque valeur correspondant à une vitesse d'évolution de concentration (le critère correspond à la somme de leurs carrés)
	double result[nbclasse];
	for (i=0;i!=nbclasse;i++)
	{
		result[i]=0;
	}
	double resultatfinal=0;

	// Calcul des flux élémentaires et stockage (le flux est enlevé à chaque classe de réactif et ajouté à chaque classe de produit)
	for (i=2;i!=nbclasse+1;i++)
	{
		for (j=1;j!=(int) floor(i/2)+1;j++)
		{
			// Ecriture à l'écran
			resultatfinal=(*noy).element(i-j,j,1)*distrib[j-1]*distrib[i-j-1]-(*noy).element(i-j,j,2)*distrib[i-1];
			result[i-j-1]=result[i-j-1]-resultatfinal;
			result[j-1]=result[j-1]-resultatfinal;
			result[i-1]=result[i-1]+resultatfinal;
		}
	}

	// Calcul du critère proprement dit
	resultatfinal=0;
	for (i=0;i!=nbclasse;i++)
	{
		resultatfinal=resultatfinal+result[i]*result[i];
	}

	return resultatfinal;		
}

// Fonction objectif pour régression à partir de distributions aux concentrations perturbées selon une loi normale (nulle quand la distribution passée en argument correspond au maximum de vraisemblance pour la distribution courante) 
double distribution::fonctionfit (distribution * distr2, double ecarttype) const
{
	int i;
	// Taille des distributions
	int taille;
	// Valeur de la fonction
	double distance=0;

	// Redimensionne la distribution à la plus grande taille des deux ajoutées
	taille = max(nbclasse,(*distr2).taille());
	
	// Calcul proprement dit
	for (i=0;i!=taille;i++)
	{
		distance = distance + pow(distrib[i]-(*distr2).element(i+1),2)/pow(distrib[i]*ecarttype,2);
	}

	distance=abs(distance/(2*taille)-0.5);

	return distance;		
}


