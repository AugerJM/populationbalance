/* Page d'introduction à la documentation des outils de Bilan de Populations */

/*! \~french @mainpage Bilan de populations
 *
 * 
 *
 * Le formalisme utilisé pour construire le code, ainsi que la documentation, est celui de la cinétique chimique, appliqué à l'aggrégation et à la fragmentation d'agrégats de particules dans un milieu fluide.
 *
 *	Le bilan de populations est représenté par le système de réactions : \f$ ( i + j ⇔ (i+j))_{(i+j)≤n} \f$ avec i et j les tailles d'agrégats en nombre de particules unitaires, et n la taille maximale d'agrégat
 * 
 *	La vitesse d'avancement (relative) de chacune de ces réaction s'écrit sous la forme : \f$ (v_{i,j} = k_{i,j} . [i].[j]-{k_{i,j}}^* . [i+j])_{(i+j)≤n}  \f$, les coefficients cinétiques \f$ k_{i,j} \f$ et \f$ {k_{i,j}}^*  \f$ étant respectivement les coefficients du noyau d'agrégation et du noyau de rupture.
 *
 * 	Le couplage entre les réactions est exprimé par : \f$ \frac {d[k]}{dt} = ∑_{i+j=k} (v_{i,j}) - ∑_{i≤n-k} (v_{i,k}) \f$



 * \~french @section intro_sec Généralités sur les modèles utilisés
 *
 * Les outils présentés dans ce paquet permettent la réalisation de bilans de populations, dans le cadre des hypothèses suivantes :
 * 	- Existence d'une classe de particules unitaires indivisibles
 * 	- Tout agrégat est formé par l'assemblage d'un nombre entier de particules unitaires (hypothèse de la discrétisation de la distribution des agrégats en multiples entiers d'une particule untitaire)
 * 	- Réactions d'aggrégation de type binaire uniquement (hypothèse d'un milieu suffisamment dilué pour rendre très improbable la coexistence de trois agrégats au même point)
 * 	- Réactions de fragmentation indépendantes des interactions entre agrégats (hypothèse d'une fragmentation uniquement due aux conditions du milieu entourant les agrégats - exclut par exemple la rupture sous l'effet d'un choc avec un autre agrégat)
 * 	- Cinétiques du premier ordre par rapport aux concentrations de chacun des réactifs (donc globalement du second ordre pour l'agrégation et du premier ordre pour la fragmentation)
 * 	- Conservation du nombre total de particules unitaires au cours du bilan (partiellement contournable - voir le paragraphe sur les limitations)



 * \~french @section struc_sec Structure générale des classes C++
 * 
 * Les classes sont regoupées dans 3 bibliothèques distinctes :	
 * 	- "Distribution" permet la représentation d'une distribution de concentrations discretes de taille indéterminée, les concentrations pouvant prendre des valeurs continues.
 * 	- "DistributionQuant" permet la représentation d'une distribution de concentrations discretes de taille indéterminée, ces concentrations ne pouvant prendre qu'un nombre fini de valeurs (quantification). L'avancement de chaque réaction élémentaire d'un unique aggrégat est alors de type déterministe (et non stochastique), le temps nécessaire à l'avancement correspondant à un temps caractéristique moyen
 * 	- "Noyau" permet la représentation d'un noyau mixte regroupant des coefficients d'aggrégation et de fragmentation (pouvant prendre des valeurs positives ou nulles). En plus des possibilités de construction de noyaux manuellement, on différencie les trois classes de noyaux qu'il est possible de générer directement : 
 * 		- les noyaux à expression purement mathématique n'ayant pas de sens physique particulier, et n'ayant pas vocation à produire des résultats réalistes (exemples : noyaux de type somme ou produit)
 * 		- les noyaux correspondant à un modèle physique spécifique, dont les paramètres sont directement des constantes physiques (exemple : noyau d'agrégation de Smoluchowski à hypothèse brownienne)
 * 		- les noyaux types correspondant à des classes de modèles physiques ayant une même expression mathématique générale, et ayant pour paramètres des constantes numériques dont l'expression en fonction de paramètres physiques dépend du modèle exact considéré (exemple : noyau de premier type turbulent, qui recouvre plusieurs variantes de modèle, différant par les phénomènes intégrés aux constantes)



 * \~french @section limit_sec Limitations
 *
 * Du fait de la nature du bilan de populations (séquentiel dans le temps, et couplage intégral des phénomènes à chaque pas de temps), la parallèlisation du code est impossible. Il est cependant facile de mener des bilans distincts en parallèle.
 *
 * Les bilans obtenus correspondent exactement au cas des agrégats constitués de particules unitaires indivisibles (classes de la distribution); à ce titre ces résultats ne peuvent a priori être étendus au cas d'agrégats dont le volume peut varier continument. Aucune théorie n'établit généralement la convergence du cas discret vers le cas continu.
 *
 * Pour obtenir une simulation rigoureusement exacte, toutes les réactions d'agrégation/rupture doivent être prises en compte : le temps de calcul pour des simulations de durées comparables évoluera donc avec le carré du nombre de classes de la distribution (l'utilisation d'astuces spécifiques pour améliorer cet aspect irait à l'encontre de la généralité des outils présentés). Il convient de noter que le bilan quantifié présente une évolution similaire de la consommation de mémoire avec la taille de la distribution.
 *
 * Les bibliothèques fournies ont été construites sur la base d'une hypothèse de conservation de la matière; même si certaines fonctions permettent de réaliser des bilans ouverts par l'ajout de conditions aux limites de type Neumann ou Dirichlet, le code n'est pas optimisé pour un tel usage, et les performances en matière de temps de calcul s'en ressentiront.  
 *
 * La version quantifiée du bilan de populations est beaucuop plus lente que la version continue, du fait de l'avancement unitaire des réactions élémentaires.



 * \~french @section install_sec Installation
 *
 * L'installation sous Linux du paquet .deb se fait simplement par la commande : dpkg -i populationbalance.deb



 * \~french @subsection tools_subsec Outils nécessaires
 *
 * Dépendances : bash, build-essential



 * \~french @subsection running Utilisation
 * 
 * Les 3 bibliothèques contenant les définitions d'objets sont directement disponibles au  niveau du système, et peuvent être utilisées dans des programmes C++ via la directive #include appliquée à Distribution.h, DistributionQuant.h et Noyau.h.
 *
 * 5 outils généraux sont utilisables en ligne de commande : ExpFit (recherche des paramètres permettant la meilleure correspondance entre un modèle et une dsitribution d'équilibre obtenue expérimentalement), KernelGen (générateurs de noyaux à partir de modèles), Kernelprod (produit de noyaux), KernelSum (somme de noyaux), PopBalEq (calcul d'un bilan de populations de type continu).
 *
 * Le code source d'une série d'exemples utilisant les différentes fonctions des bibliothèques est disponible.



 * \~french @section copyright Copyright et License
 * Bla bla bla...
 * <a href="http://test.com/"> Site... </a href="http://test.com/">
 *
 */



/*! \~english @mainpage Population balance
 *
 * 
 *
 * The mathematical formulation used for both code and documentation is that of chemical kinetics, applied to aggregation and breakage of particles clusters in a fluid medium.
 *
 *	Population balance is expressed by the following equations system : \f$ ( i + j ⇔ (i+j))_{(i+j)≤n} \f$ with i and j the clusters sizes (as a number of unitary particles), and n the maximal allowed cluster size
 * 
 *	Relative advancement rate for each elementary reaction is giiven by : \f$ (v_{i,j} = k_{i,j} . [i].[j]-{k_{i,j}}^* . [i+j])_{(i+j)≤n}  \f$, the kinetics coefficients \f$ k_{i,j} \f$ and \f$ {k_{i,j}}^*  \f$ being the aggregation and breakage kernles coefficients respectively.
 *
 * 	Coupling between reactions is expressed by : \f$ \frac {d[k]}{dt} = ∑_{i+j=k} (v_{i,j}) - ∑_{i≤n-k} (v_{i,k}) \f$



 * \~english @section intro_sec General informations about models
 *
 * Tools from this package allow computation of population balance models, with the following hypotheses :
 * 	- Existence of a minimal particle size (unitary particles)
 * 	- Each cluster consists of a number (integer) of aggregated unitary particles (this hypothesis corresponds to a discretization of clusters distribution in multiples of unitary particles)
 * 	- Binary aggregation reactions (this hypothesis implies a diluted enough medium to make the meeting of 3 clusters at the same point highly improbable)
 * 	- Breakage reactions independent from clusters interactions (hypothesis of breakage reactions only caused by fluid medium movement - precludes for example breakage as a result of clusters collision)
 * 	- First order kinetics with respect to reactants concentrations (global second order kinetics for aggregation, global first order kinetics for breakage)
 * 	- Unitary particles conservation during population balance computation (this is not a hardcoded limitation - see Limitations section)



 * \~english @section struc_sec General structure of C++ classes
 * 
 * Classes are arranged in 3 distinct libraries :	
 * 	- "Distribution" allows the representation of discrete concentration distributions, with continuous concentrations values
 * 	- "DistributionQuant" allows the representation of discrete concentration distributions, with discrete concentrations values (quantification). Advancement for a unique aggregate in every elementary reaction is deterministic, the corresponding duration being a mean characteristic time step.
 * 	- "Noyau" allows the representation of dual kernels, consisting of aggregation and breakage coefficients both (values can be positive only). In addition to manual kernels construction, three general kernel types can be generated :
 * 		- purely mathematical kernel expressions, with no physical nor realistic meaning (examples : sum or product kernels)
 * 		- kernels corresponding to specific physical models, with physical constants as parameters (example : Smoluchowski with brownian hypothesis aggregation kernel)
 * 		- kernel types corresponding to physical models classes, showing a same general mathematical expression, and having numerical parameters. The exact expression of parameters as functions of physical values depends on the physical model considered (example : first type turbulent aggregation kernel, covering several models differing by which phenomena is integrated in its constants)



 * \~english @section limit_sec Limitations
 *
 * Du fait de la nature du bilan de populations (séquentiel dans le temps, et couplage intégral des phénomènes à chaque pas de temps), la parallèlisation du code est impossible. Il est cependant facile de mener des bilans distincts en parallèle.
 *
 * Les bilans obtenus correspondent exactement au cas des agrégats constitués de particules unitaires indivisibles (classes de la distribution); à ce titre ces résultats ne peuvent a priori être étendus au cas d'agrégats dont le volume peut varier continument. Aucune théorie n'établit généralement la convergence du cas discret vers le cas continu.
 *
 * Pour obtenir une simulation rigoureusement exacte, toutes les réactions d'agrégation/rupture doivent être prises en compte : le temps de calcul pour des simulations de durées comparables évoluera donc avec le carré du nombre de classes de la distribution (l'utilisation d'astuces spécifiques pour améliorer cet aspect irait à l'encontre de la généralité des outils présentés). Il convient de noter que le bilan quantifié présente une évolution similaire de la consommation de mémoire avec la taille de la distribution.
 *
 * Les bibliothèques fournies ont été construites sur la base d'une hypothèse de conservation de la matière; même si certaines fonctions permettent de réaliser des bilans ouverts par l'ajout de conditions aux limites de type Neumann ou Dirichlet, le code n'est pas optimisé pour un tel usage, et les performances en matière de temps de calcul s'en ressentiront.  
 *
 * La version quantifiée du bilan de populations est beaucoup plus lente que la version continue, du fait de l'avancement unitaire des réactions élémentaires.



 * \~english @section install_sec Installation
 *
 * Package installation on Linux is easily done using the following command : dpkg -i populationbalance.deb



 * \~english @subsection tools_subsec Needed tools
 *
 * Dependencies : bash, build-essential



 * \~english @subsection running Running
 * 
 * Once installed, the three libraries containing classes definitions are avaiable system-wide, and can easily be used in C++ programs through the #include tag applied to Distribution.h, DistributionQuant.h and Noyau.h.
 *
 * 5 general use tools are usable through command line : ExpFit (parameters fit for optimizing a kernel model with respect to a given steady state experimental distribution), KernelGen (generator for kernels linked to theoretical models), Kernelprod (kernels product), KernelSum (kernels sum), PopBalEq (continuous concentrations population balance computation).
 *
 * Sources for several examples making use of libraries classes and functions.



 * \~english @section copyright Copyright and License
 * Bla bla bla...
 * <a href="http://test.com/"> Site... </a href="http://test.com/">
 *
 */