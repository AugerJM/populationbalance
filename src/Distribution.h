///\~french @file      	Distribution.h
///\~french @author    	JMA
///\~french @version   	1.0
///\~french @date      	30/06/2017
///\~french @brief     	Définit une distribution 
///\~french @details   	Cette classe définit une distribution de concentration de particules selon leur taille

///\~english @file      	Distribution.h
///\~english @author    	JMA
///\~english @version   	1.0
///\~english @date      	30/06/2017
///\~english @brief     	Defines a distribution 
///\~english @details   	This class defines a distribution of clusters concentration by clusters size					 

#ifndef DISTRIBUTION_H
#define DISTRIBUTION_H

#include <string>
#include <vector>
#include "Noyau.h"

class noyau;

/// \~french @brief      Objet : Distribution en concentration unitaire volumique
/// \~french @details    Contient les concentrations des différentes classes d'agrégats, ainsi que les opérations standard.

/// \~english @brief      Object : Clusters unitary volumic concentrations distribution
/// \~english @details    Contains concentrations for varying cluster sizes, and standard operations.

class distribution 
{
	private:
	

	/// \~french @brief 	 	Nombre de classes définies dans le noyau (en partant de la classe de particules unitaires).
	/// \~french @details    	Les coefficients correspondant à des classes de supérieures sont considérées nuls (classes non réactives).

	/// \~english @brief 	  	Number of kernel cluster classes (starting from unitary cluster).
	/// \~english @details   	Concentrations of bigger clusters are considered egal to 0 (non-reactive cluster classes).
	int nbclasse;

	/// \~french @brief 		Tableau de la distribution
	/// \~english @brief 		Distribution table
	std::vector<double> distrib;

	public:
	
	/// \~french @brief      	Constructeur de base

	/// \~english @brief     	Base constructor
	distribution (void);
	

	/// \~french @brief      	Constructeur de la distribution à partir d'un fichier
	/// \~french @details    	Les valeurs sont séparées par des espaces, et commencent à la classe unitaire. Note : permet de reprendre un calcul sauvegardé. 
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Distribution constructor from file
	/// \~english @details    	Values are separated by spaces, and begin from unitary clusters. Note : allows to restart a stored computation. 
	/// \~english @param    	nom		File name
	distribution (std::string nom);


	/// \~french @brief     	Constructeur de copie de la distribution
	/// \~french @param    		dis		Pointeur vers la distribution à copier

	/// \~english @brief     	Copy constructor for distribution
	/// \~english @param    	dis		Pointer to distribution to copy
	distribution (const distribution &dis);


	/// \~french @brief      	Ecriture de la distribution à l'écran
	/// \~english @brief      	Writes distribution on screen
	void ecrire () const;


	/// \~french @brief      	Ecriture de la distribution dans un fichier
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par la classe de particules unitaires.
	///				Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Writes distribution in file
	/// \~english @details    	Concentrations are written in lines, starting from unitary clusters.
	///				Note : file is open in append mode.
	/// \~english @param    	nom		Filename
	void ecrire (std::string nom) const;


	/// \~french @brief      	Ecriture de la distribution dans un fichier (variante avec enregistement du pas de temps)
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par la classe de particules unitaires. La première valeur correspond au pas de temps (utile en cas de pas de temps variable).
	///			    	Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		nom		Nom du fichier
	/// \~french @param    		temps		Pas de temps à indiquer

	/// \~english @brief      	Writes distribution in file (variant including time step information)
	/// \~english @details    	Concentrations are written in lines, starting from unitary clusters. First value in each line corresponds to time step (useful for variable time steps).
	///			    	Note : file is open in append mode.
	/// \~english @param    	nom		Filename
	/// \~english @param    	temps		Timestep to append
	void ecrire (std::string nom, double temps) const;


	/// \~french @brief      	Affichage de la concentration totale en particules élémentaires de la distribution à l'écran

	/// \~english @brief      	Writes the distribution total unitary clusters concentration on screen
	void population (void) const;


	/// \~french @brief      	Renvoie la population totale en particules de la distribution
	/// \~french @details    	Utilisé en interne pour ajuster les calculs de distances entre distributions.
	/// \~french @return    	Concentration totale en particules élémentaires

	/// \~english @brief      	Returns the distribution total unitary clusters concentration
	/// \~english @details    	Used in internal computations involving distribution distances.
	/// \~english @return    	Total unitary clusters concentration in the distribution
	double populationval (void) const;


	/// \~french @brief     	Renvoie l'intégrale de la distribution
	/// \~french @details    	Utilisé en interne pour ajuster les calculs de distances entre distributions.
	/// \~french @return    	Concentration totale en particules (en nombre indépendamment de leur taille)

	/// \~english @brief      	Returns the distribution integral
	/// \~english @details    	Used in internal computations involving distribution distances.
	/// \~english @return    	Distribution total clusters concentration (clusters number per volume independantly from cluster sizes)
	double integrale (void) const;


	/// \~french @brief      	Calcul de la distribution au pas de temps suivant (met à jour la distribution);
	/// \~french @details    	Si le pas de temps imposé conduit à des concentrations négatives, le calcul est réalisé pour le pas de temps maximal évitant cette configuration, qui est renvoyé par la fonction de manière à assurer un suivi.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		pastps		Pas de temps (durée en s)
	/// \~french @return    	Taille de pas maximale compatible avec des concentrations maintenues positives (<= pas de temps passé en paramètre)

	/// \~english @brief      	Advancement to next timestep (updates distribution);
	/// \~english @details    	If the chosen timestep would lead to negative concentrations, the highest timestep avoiding this situation is used instead and returned.    
	/// \~english @param    	noy		Pointer to kernel used during this timestep
	/// \~english @param    	pastps		Timestep (duration in s)
	/// \~english @return    	Maximal timestep allowing positive concentrations (<= timestep passed as parameter)	
	double incremente (noyau * noy, double pastps);


	/// \~french @brief      	Calcul de la distribution au pas de temps suivant (met à jour la distribution - méthode Runge-Kutta 2);
	/// \~french @details    	Si le pas de temps imposé conduit à des concentrations négatives, le calcul est réalisé pour le pas de temps maximal évitant cette configuration, qui est renvoyé par la fonction de manière à assurer un suivi.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		pastps		Pas de temps (durée en s)
	/// \~french @return    	Taille de pas maximale compatible avec des concentrations maintenues positives (<= pas de temps passé en paramètre)

	/// \~english @brief      	Advancement to next timestep (updates distribution - Runge-Kutta 2 method);
	/// \~english @details    	If the chosen timestep would lead to negative concentrations, the highest timestep avoiding this situation is used instead and returned.    
	/// \~english @param    	noy		Pointer to kernel used during this timestep
	/// \~english @param    	pastps		Timestep (duration in s)
	/// \~english @return    	Maximal timestep allowing positive concentrations (<= timestep passed as parameter)	
	double incrementeRK2 (noyau * noy, double pastps);


	/// \~french @brief      	Calcul de la distribution au pas de temps suivant (met à jour la distribution);
	/// \~french @details    	Si le pas de temps conduit à des concentrations négatives, le calcul considère que les flux de matière correspondant à ces classes n'existent qu'une partie du pas de temps considéré (évite la réduction à des pas de temps quasi-nuls que la méthode incremente produit dans le cas de noyaux fortement hétérogènes)
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		pastps		Pas de temps (durée en s)
	/// \~french @return    	Taille de pas maximale compatible avec des concentrations maintenues positives (<= pas de temps passé en paramètre)

	/// \~english @brief      	Advancement to next timestep (updates distribution);
	/// \~english @details    	If the chosen timestep would lead to negative concentrations, the corresponding elementary reactions are considered active only during part of the timestep (this variant avoids the problem of very small adjusted timesteps when using the basic increment function with very heterogeneous kernels)   
	/// \~english @param    	noy		Pointer to kernel used during this timestep
	/// \~english @param    	pastps		Timestep (duration in s)
	/// \~english @return    	Maximal timestep allowing positive concentrations (<= timestep passed as parameter)
	double incrementeava (noyau * noy, double pastps);


	/// \~french @brief      	Calcul de la distribution au pas de temps précédent (met à jour la distribution);
	/// \~french @details    	Si le pas de temps conduit à des concentrations négatives, le calcul considère que les flux de matière correspondant à ces classes n'existent qu'une partie du pas de temps considéré (évite la réduction à des pas de temps quasi-nuls que la méthode incremente produit dans le cas de noyaux fortement hétérogènes)
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		pastps		Pas de temps (durée en s)
	/// \~french @return    	Taille de pas maximale compatible avec des concentrations maintenues positives (<= pas de temps passé en paramètre)

	/// \~english @brief      	Advancement to previous timestep (updates distribution);
	/// \~english @details    	If the chosen timestep would lead to negative concentrations, the corresponding elementary reactions are considered active only during part of the timestep (this variant avoids the problem of very small adjusted timesteps when using the basic increment function with very heterogeneous kernels)   
	/// \~english @param    	noy		Pointer to kernel used during this timestep
	/// \~english @param    	pastps		Timestep (duration in s)
	/// \~english @return    	Maximal timestep allowing positive concentrations (<= timestep passed as parameter)
	double incrementeinv (noyau * noy, double pastps);


	/// \~french @brief      	Renvoie la concentration en particules de la classe i
	/// \~french @param    		i		Classe de particules (nombre de particules élémentaires par particule, >0)
	/// \~french @return    	Concentration (en nombre d'agrégats par  volume) dans la classe i

	/// \~english @brief      	Returns the concentration of size i clusters
	/// \~english @param    	i		Cluster size (number of unitary particles in cluster, >0)
	/// \~english @return    	Concentration (number per volume) of size i clusters
	double element (int i) const;


	/// \~french @brief      	Renvoie le nombre de classes dans la distribution
	/// \~french @return    	Nombre total de classes (>0)

	/// \~english @brief      	Return the maximal clusters size in distribution
	/// \~english @return    	Maximal clusters size (unitary clusters, >0)
	int taille (void) const;


	/// \~french @brief      	Ajout d'une autre distribution
	/// \~french @details    	L'ajout se fait à la distribution en cours (l'opération ne crée pas de nouvelle distribution-somme). Lors de l'opération, la distribution est redimensionnée au plus grand nombre de classes des 2 distributions ajoutées. 
	/// \~french @param    		distr2		Pointeur vers la distribution à ajouter

	/// \~english @brief      	Adds another distribution
	/// \~english @details    	The other distribution is added to the current one (the resulting sum is not created as a third distribution). During this operation, the distribution is resized to correspond to the greater clusters size from the 2 summed distributions.
	/// \~english @param    	distr2		Pointer the distribution to add to the current one
	void ajout (distribution * distr2);


	/// \~french @brief      	Calcul de distances entre 2 distributions
	/// \~french @details    	Plusieurs définitions de la distance sont disponibles via le paramètre type.  
	/// \~french @param    		distr2		Pointeur vers la distribution dont la distance est à mesurer	
	/// \~french @param    		type		Indicateur de la distance utilisée ("TVD" : Total Variation Distance - "Hellinger" : Hellinger - "Bhattacharyya" : Bhattacharyya - "Manhattan" : Manhattan)

	/// \~english @brief      	Computes the distance between 2 distributions
	/// \~english @details    	Various distance definitions are usable through the "type" parameter.  
	/// \~english @param    	distr2		Pointer to the distribution with which the distance must be calculated	
	/// \~english @param    	type		Indicator of the distance definition used ("TVD" : Total Variation Distance - "Hellinger" : Hellinger - "Bhattacharyya" : Bhattacharyya - "Manhattan" : Manhattan) 
	double distance (distribution * distr2, std::string type) const;


	/// \~french @brief      	Calcul et affichage à l'écran des flux de matière correspondant aux équilibres élémentaires
	/// \~french @details    	Les valeurs obtenues sont fortement dépendantes du noyau passé en paramètre. Le pas de temps est normalisé à 1.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les flux

	/// \~english @brief      	Computation of instant matter flux corresponding to each elementary aggregation/fragmentation equilibrium, which are then written on screen
	/// \~english @details    	Values highly dependent on the kernel used for computation. Time step is normalized to 1.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	void flux (noyau * noy) const;


	/// \~french @brief      	Ecriture des flux de matière correspondant aux équilibres élémentaires dans un fichier
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par les flux entre particules les plus petites.
	///			    	Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Computation of instant matter flux corresponding to each elementary aggregation/fragmentation equilibrium, which are then written in a file
	/// \~english @details    	Values are written in lines, starting from equilibirums involving the smaller clusters.
	///			    	Note : file is open in append mode.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~english @param    	nom		Filename
	void flux (noyau * noy, std::string nom) const;


	/// \~french @brief      	Dilution/concentration du milieu représenté par la distribution
	/// \~french @details    	Multiplication des concentrations de la distribution par une constante.
	/// \~french @param    		facteur		Facteur mutiplicatif appliqué (>1 - concentration, <1 - dilution)

	/// \~english @brief      	Applies a dilution or concentration increase in the medium simulated by the distribution
	/// \~english @details    	Multiplies all distribution concentrations by the same constant.
	/// \~english @param    	facteur		Applied multiplicative constant (>1 - concentration increase, <1 - dilution)
	void dilution (float facteur);


	/// \~french @brief      	Calcul et affichage à l'écran des constantes d'équilibres normalisées des réactions élémentaires
	/// \~french @details    	Les valeurs obtenues sont fortement dépendantes du noyau passé en paramètre.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre

	/// \~english @brief      	Computation of normalized equilibrium constants corresponding to each elementary aggregation/fragmentation, which are then written on screen
	/// \~english @details    	Values highly dependent on the kernel used for computation. Time step is normalized to 1.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	void equilibres (noyau * noy) const;


	/// \~french @brief      	Ecriture des constantes d'équilibres normalisées des réactions élémentaires dans un fichier
	/// \~french @details    	Les valeurs sont écrites en ligne, en commençant par les constantes d'équilibre entre particules les plus petites.
	///			    	Note : le fichier est ouvert en mode ajout.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @param    		nom		Nom du fichier

	/// \~english @brief      	Computation of normalized equilibrium constants corresponding to each elementary aggregation/fragmentation, which are then written in file
	/// \~english @details    	Values are written in lines, starting from equilibirums involving the smaller clusters.
	///			    	Note : file is open in append mode.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~english @param    	nom		Filename
	void equilibres (noyau * noy, std::string nom) const;


	/// \~french @brief     	Calcul d'un critère de stabilité de la distribution avec un noyau donné
	/// \~french @details    	Le critère actuel sert pour des tests (vérification de sa validité). Il est positif et ne s'annule qu'à l'équilibre
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les constantes d'équilibre
	/// \~french @return    	Valeur du critère

	/// \~english @brief      	Computation of a stability criterion for the distribution evolution under a given kernel
	/// \~english @details    	Criterion is under tests (validity). Values are positive, and reach zero only when the distribution is balanced under a given kernel
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~french @return    	Criterion value
	double critere (noyau * noy) const;


	/// \~french @brief      	Effet de l'avancement d'une réaction élémentaire sur la distribution ( i + j -> (i+j) )
	/// \~french @details    	Pas de sécurité sur cette méthode (les concentrations peuvent passer en négatif). Utilité : tester le voisinage de la distribution tout an ayant conservation de la matière
	/// \~french @param    		i		Première taille de cluster de l'équilibre à avancer
	/// \~french @param    		j		Seconde taille de cluster de l'équilibre à avancer
	/// \~french @param    		av		Avancement imposé (en particules par unité de volume, peut être négatif)

	/// \~english @brief      	Displaces the elementary aggregation reaction ( i + j -> (i+j) )
	/// \~english @details    	This method is not secured (concnetrations can reach negative values). Uses : easy access to the distribution neighbourhood while still have unitary clusters conservation
	/// \~english @param    	i		First clusters size involved in the reaction to advance
	/// \~english @param    	j		Second clusters size involved in the reaction to advance
	/// \~english @param    	av		Displacement (clusters number per volume, can be negative)
	void avancement (int i, int j, double av);


	/// \~french @brief      	Condition limite de type Neumann (impose une concentration dans une classe) - renvoie le flux de matière instantané nécessaire pour équilibrer la classe
	/// \~french @details    	Permet de produire un bilan de population plus général (sans conservation de la matière - équilibre en flux)
	///				Si le numéro de classe demandé est impossible, la condition est affectée à la classe viable la plus proche (donc la plus grande ou la plus petite)
	///			    	Note : l'utilisation dans le cadre d'un bilan impose d'alterner l'incrémentation avec une ou plusieurs conditions aux limites
	/// \~french @param    		i		Taille de cluster dont la concentration est imposée
	/// \~french @param    		val		Concentration imposée
	/// \~french @return    	Nombre d'agrégats par unité de volume à ajouter pour assurer la condition (peut être négatif)

	/// \~english @brief      	Neumann condition (forces concentration of one cluster size in distribution) - returns the instant clusters flux needed to balance out clusters concentration
	/// \~english @details    	Allows more general population balance computations (without matter conservation - balanced in flux)
	///				If the cluster size affected does not exist in the distribution, Neumann condition is applied to the nearest one (either the biggest or smallest clusters)
	///			    	Note : use during population balance computation involves alternating time incrementation with the appliation of one or more limit conditions 
	/// \~english @param    	i		Cluster size which concentration is to be imposed
	/// \~english @param    	val		Concentration imposed
	/// \~english @return    	Number of aggregates per volume to add to cluster concentration in order to apply the condition
	double neumann (int i, double val);


	/// \~french @brief      	Condition limite de type Dirichlet (impose une variation de concentration dans une classe) - renvoie le pas de temps maximal pour assurer que la concentration reste positive (<= au pas de  temps imposé)
	/// \~french @details    	Permet de produire un bilan de population plus général (sans conservation de la matière - équilibre en flux)
	///				Si le numéro de classe demandé est impossible, la condition est affectée à la classe viable la plus proche (donc la plus grande ou la plus petite)
	///			    	Note : l'utilisation dans le cadre d'un bilan impose d'alterner l'incrémentation avec une ou plusieurs conditions aux limites
	/// \~french @param    		i		Taille de cluster dont la variation de concentration est imposée
	/// \~french @param    		var		Variation de concentration imposée (par unité de temps)
	/// \~french @param    		pastps		Pas de temps (doit être identique à celui de l'incrémentation)
	/// \~french @return    	Pas de temps maximal pour assurer que la concentration reste positive

	/// \~english @brief      	Dirichlet condition (forces concentration variation of one cluster size in distribution) - returns the maximum timestep ensuring cluster concentration is still positive (<= to the applied timestep)
	/// \~english @details    	Allows more general population balance computations (without matter conservation - balanced in flux)
	///				Si le numéro de classe demandé est impossible, la condition est affectée à la classe viable la plus proche (donc la plus grande ou la plus petite)
	///			    	Note : use during population balance computation involves alternating time incrementation with the appliation of one or more limit conditions 
	/// \~english @param    	i		Cluster size to which concentration variation is to be imposed
	/// \~english @param    	var		Concentration variation imposed (per time)
	/// \~english @param    	pastps		Timestep (must be identical to )
	/// \~english @return    	Effective timestep ensuring cluster concentration is still positive
	double dirichlet (int i, double var, double pastps);


	/// \~french @brief      	Calcul et renvoi d'un critère valant 0 quand le noyau passé en argument est compatible avec l'état d'équilibre décrit par la distribution (correspond à la vitesse d'évolution totale des concentrations)
	/// \~french @details    	Les valeurs obtenues sont fortement dépendantes du noyau passé en paramètre. Le pas de temps est normalisé à 1.
	/// \~french @param    		noy		Pointeur vers le noyau utilisé pour calculer les flux
	/// \~french @return    	Valeur du critère (positif ou nul)

	/// \~english @brief      	Returns a positive criterion which value is zero when a kernel is compatible with the distribution as an equilibrium state (corresponds to the sum of concentrations rates)
	/// \~english @details    	Values highly dependent on the kernel used for computation. Time step is normalized to 1.
	/// \~english @param    	noy		Pointer to the kernel used for the computation
	/// \~english @return    	Criterion value (positive)
	double critcompat (noyau * noy) const;

	/// \~french @brief      	Fonction de maximum de vraisemblance pour une perturbation normale de la distribution
	/// \~french @details    	Cette fonction est minimale lorsque la distribution passée en argument est la distribution non perturbée la plus probable correspondant à la distribution courante. 
	///				La distribution courante est supposée correspondre à une distribution initiale dont les concentrations ont subi l'ajout d'un bruit gaussien proportionnel. 
	///				Cette approche tente de modéliser au mieux l'effet d'erreurs de mesures sur les concentration de la distribution courante. 
	///				L'expression de la fonction vaut : \f$ F = \left| \left( \sum_{i=1}^N \left( \frac{[i]_{courante}-[i]_{argument}}{ecarttype \cdot [i]_{courante}} \right) ^2 \right) \cdot \frac{1}{2 \cdot N} - 0.5 \right| \f$
	/// \~french @param    		distr2		Pointeur vers la distribution dont on mesure la vraisemblance
	/// \~french @param    		ecarttype	Ecart-type de la perturbation de la distribution courante (en fraction des concentrations en clusters) 
	/// \~french @return    	Valeur de la fonction (positive ou nulle)

	/// \~english @brief      	Maximum likelyhood function for normally distributed error clusters distribution  
	/// \~english @details    	The function is minimal when the argument distribution corresponds to the current distribution with gaussian noise removed with the highest probability. 
	///				The current distribution is supposed to correspond to a starting distribution with proportional gaussian noise added to cluster concentrations. 
	///				This method tries to model realistic measurement errors for individual clusters concentrations. 
	///				The function is expressed as : \f$ F = \left| \left( \sum_{i=1}^N \left( \frac{[i]_{current}-[i]_{argument}}{ecarttype \cdot [i]_{current}} \right) ^2 \right) \cdot \frac{1}{2 \cdot N} - 0.5 \right| \f$
	/// \~english @param    	distr2		Pointer the distribution which likelyhood is to be measured
	/// \~english @param    	ecarttype	Standard deviation from the current distribution (as a fraction of clusters concentrations values
	/// \~english @return    	Function value 	(positive)
	double fonctionfit (distribution * distr2, double ecarttype) const;
};
#endif