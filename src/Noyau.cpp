#include "Noyau.h"
#include "Distribution.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <cstring>
#include <vector>
#include <time.h>
#include <omp.h>
#include <cmath>
#include <random>

using namespace std;

// Objet : Noyau d'aggregation/dislocation
// Méthodes : 	noyau - construction
//		element - recupération éléments noyau
//		ecrire - affichage ou enregistrement noyau
//		ajout - ajoute les éléments d'un autre noyau à celui-ci
//		taille - renvoie le nombre de classes du noyau

	
// Nombre de classes (noyau supposé nul au-delà, à partir de 1)
//int nbclasses;
// Noyau d'aggrégation (sous forme linéiare pour éviter le stockage inutile des éléments symétriques
//std::vector<double> noyauaggr;
// Noyau de dislocation
//vector<double> noyaudisl;
	
	
// Constructeur de base
noyau::noyau (void)
{
	nbclasses=0;
}
	
// Constructeur à partir d'un fichier (aggregation - 1 ou dislocation - 2 , l'autre étant nul)
noyau::noyau (std::string nom, int categorie)
{
	int i;
	string::size_type emplacement;
	nbclasses=0;
	ifstream fichier;
	fichier.open(nom.c_str(), ios::app);
	if (categorie == 1)
	{
		if(fichier.is_open())
		{    
			string ligne;
			while (getline(fichier,ligne))
			{
				nbclasses++;
				for (i=1;i!=nbclasses+1;i++)
				{
					emplacement = ligne.find(' ');
					noyauaggr.push_back(atof((ligne.substr(0, emplacement)).c_str()));
					ligne = ligne.substr(emplacement + 1);
				}
			}	
       	fichier.close();
		}
		else
		{
			cout << "Impossible d'ouvrir le fichier !" << endl;
		}
	}	
	else
	{
				if(fichier.is_open())
		{    
			string ligne;
			while (getline(fichier,ligne))
			{
				nbclasses++;
				for (i=1;i!=nbclasses+1;i++)
				{
					emplacement = ligne.find(' ');
					noyaudisl.push_back(atof((ligne.substr(0, emplacement)).c_str()));
					ligne = ligne.substr(emplacement + 1);
				}
			}	
       	fichier.close();
		}
		else
		{
			cout << "Impossible d'ouvrir le fichier !" << endl;
		}
	}
}

// Constructeur à partir de 2 fichiers (aggregation puis dislocation)
noyau::noyau (std::string nom, std::string nom2)
{
	int i;
	string::size_type emplacement;
	int nbclasses1=0;
	int nbclasses2=0;
	ifstream fichier, fichier2;
	fichier.open(nom.c_str(), ios::app);
	fichier2.open(nom2.c_str(), ios::app);

	if(fichier.is_open())
	{    
		string ligne;
		while (getline(fichier,ligne))
		{
			nbclasses1++;
			for (i=1;i!=nbclasses1+1;i++)
			{
				emplacement = ligne.find(' ');
				noyauaggr.push_back(atof((ligne.substr(0, emplacement)).c_str()));
				ligne = ligne.substr(emplacement + 1);
			}
		}	
   	fichier.close();
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier !" << endl;
	}
	if(fichier2.is_open())
	{    
		string ligne;
		while (getline(fichier2,ligne))
		{
			nbclasses2++;
			for (i=1;i!=nbclasses2+1;i++)
			{
				emplacement = ligne.find(' ');
				noyaudisl.push_back(atof((ligne.substr(0, emplacement)).c_str()));
				ligne = ligne.substr(emplacement + 1);
			}
		}	
    	fichier.close();
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier !" << endl;
	}
	nbclasses=max(nbclasses1,nbclasses2);	
}
		
// Ecriture des éléments du noyau à l'écran
void noyau::ecrire (void) const
{
	int i,j;
	// Ecriture en triangle noyau aggrégation
	cout << "Noyau aggregation Kij (symetrique)" << endl << endl;
	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			cout << "K" << j+1 << "-" << i+1 << " " << noyauaggr[j+i*(i+1)/2] << "\t";
		}
		cout << endl;
	}
	// Ecriture en triangle noyau dislocation
	cout << endl << "Noyau dislocation Ki->j" << endl << endl;
	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			cout << "K" << i+j+2 << "->" << j+1 << "\t" << noyaudisl[j+i*(i+1)/2] << "\t";
		}
		cout << endl;
	}
}

// Ecriture des éléments du noyau dans un fichier (nom - nom du fichier, categorie - type du noyau; 1-aggrégation, 2-dislocation) 
void noyau::ecrire (std::string nom, int categorie) const
{	
	// Remplacement dans le fichier
	ofstream fichier;
	fichier.open(nom.c_str(), ios::out); 
	if(fichier.is_open())
	{    
		int i,j;
		
		// Cas du noyau d'aggrégation
		if (categorie == 1)
		{
			for (i=0;i!=nbclasses;i++)
			{
				for (j=0;j!=i+1;j++)
				{
					fichier << noyauaggr[j+i*(i+1)/2] << " ";
				}
				fichier << endl;
			}
			fichier.close();
		}
		else
		{
			for (i=0;i!=nbclasses;i++)
			{
				for (j=0;j!=i+1;j++)
				{
					fichier << noyaudisl[j+i*(i+1)/2] << " ";
				}
				fichier << endl;
			}
			fichier.close();
		}
	}
	else
	{
            cout << "Impossible d'ouvrir le fichier !" << endl;
	}
}
	
// Récupération des éléments du noyau (coordonnées i et j, type : 1 - aggrégation, 2 - dislocation)
double noyau::element(int i, int j, unsigned int categorie) const
{
	int a=max(i,j);
	int b=min(i,j);
	// Si on cherche les éléments d'aggrégation
	if (categorie ==1)
	{
		// Si les indices sont dans la partie définie du noyau, on les renvoie
		if (i<=nbclasses && j<=nbclasses)
		{
			return noyauaggr[b-1+a*(a-1)/2];
		}
		// Sinon cet élément est nul
		else
		{
			return 0;
		}
	}
	// Si on cherche les éléments de dislocation  
	else
	{
		// Si les indices sont dans la partie définie du noyau, on les renvoie (dislocation de i+j vers j et i)
		if (i<=nbclasses && j<=nbclasses)
		{
			return noyaudisl[b-1+a*(a-1)/2];
		}
		// Sinon cet élément est nul
		else
		{
			return 0;
		}
	}
}
	
// Récupération de la taille du noyau (va servir pour les opérations entre noyaux)
int noyau::taille(void) const
{
	return nbclasses;
}
	
// Ajout d'un autre noyau
void noyau::ajout(noyau * noy2)
{
	int i,j;

	// Redimensionne les noyaux aggrégation et dislocation à la plus grande taille des deux ajoutées
	nbclasses = max(nbclasses,(*noy2).taille());
	noyauaggr.resize(nbclasses*(nbclasses+1)/2,0);
	noyaudisl.resize(nbclasses*(nbclasses+1)/2,0);

	// Somme
	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			noyaudisl[j+i*(i+1)/2]=noyaudisl[j+i*(i+1)/2]+(*noy2).element(i+1,j+1,2);
			noyauaggr[j+i*(i+1)/2]=noyauaggr[j+i*(i+1)/2]+(*noy2).element(i+1,j+1,1);
		}
	}
}

// Multiplication par un autre noyau
void noyau::produit(noyau * noy2)
{
	int i,j;

	// Redimensionne les noyaux aggrégation et dislocation à la plus grande taille des deux multipliées
	nbclasses = max(nbclasses,(*noy2).taille());
	noyauaggr.resize(nbclasses,0);
	noyaudisl.resize(nbclasses,0);

	// Produit
	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			noyaudisl[j+i*(i+1)/2]=noyaudisl[j+i*(i+1)/2]*(*noy2).element(i+1,j+1,2);
			noyauaggr[j+i*(i+1)/2]=noyauaggr[j+i*(i+1)/2]*(*noy2).element(i+1,j+1,1);
		}
	}
}

// Passage à la puissance
void noyau::puissance(double exposant)
{
	int i,j;

	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			noyaudisl[j+i*(i+1)/2]=pow(noyaudisl[j+i*(i+1)/2], (double) exposant);
		}
	}
}

// Echange
void noyau::echange(void)
{
	int i,j;
	double temp;

	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			temp=noyaudisl[j+i*(i+1)/2];
			noyaudisl[j+i*(i+1)/2]=noyauaggr[j+i*(i+1)/2];
			noyauaggr[j+i*(i+1)/2]=temp;
		}
	}
}

// Objet : Noyau d'aggregation/dislocation constant
// Constructeur de noyaux constants (nb de classes donné par taille)
noyauConst::noyauConst (double aggr, double disl, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Création des noyaux d'aggrégation et dislocation (noyau nul = aucun effet)
	// Redimensionne le tableau du noyau aggr et le remplit
	noyauaggr.resize(taille*(taille+1)/2,0);
	// Redimensionne le tableau du noyau disl et le remplit
	noyaudisl.resize(taille*(taille+1)/2,0);
	
	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=aggr;
				noyaudisl[j+i*(i+1)/2]=disl;
			}
		}
	}
	
	
}



// Objet : Noyau d'aggregation pure Smoluchowski brownien
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, T - température, mu - viscosité dynamique, taille - nb de classes, modèle utilisé : brownien de type Smoluchowski )
noyauSmolBrown::noyauSmolBrown (double r, double T, double mu, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Noyau brownien de type Smoluchowski (pure aggregation, particules sphériques parfaitement denses)

	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=2*T*1.38*pow(10,-23)*( pow((j+1)*pow(r,3),(double) 1/3) + pow((i+1)*pow(r,3),(double) 1/3) ) * ( 1/(pow((j+1)*pow(r,3),(double) 1/3)) + 1/(pow((i+1)*pow(r,3),(double) 1/3)) )/(3*mu);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}



// Objet : Noyau d'aggregation pure Smoluchowski laminaire
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, cis - vitesse de cisaillement, taille - nb de classes)
noyauSmolLam::noyauSmolLam (double r, double cis, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=4*cis*pow(pow((j+1)*pow(r,3),(double) 1/3) + pow((i+1)*pow(r,3),(double) 1/3) ,3)/3;
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}


/*
// Objet : Noyau d'aggregation pure Fuchs brownien (Smoluchowski + coefficient d'efficacité de collage)
class noyauFuchs : public noyau
{




};
*/


// Objet : Noyau d'aggregation pure Camp et Stein
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, nu - viscosité cinématique, epsilon - vitesse de dissipation d'énergie massique, taille - nb de classes)
noyauCampStein::noyauCampStein (double r, double nu, double epsilon, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=4*pow(epsilon/nu,0.5)*pow(pow((j+1)*pow(r,3),(double) 1/3) + pow((i+1)*pow(r,3),(double) 1/3) ,3)/3;
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}


// Objet : Noyau d'aggregation pure Saffman et Turner 
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, nu - viscosité cinématique, epsilon - vitesse de dissipation d'énergie massique, taille - nb de classes)
noyauSaffTurner::noyauSaffTurner (double r, double nu, double epsilon, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=pow(8*3.14156/15,0.5)*pow(epsilon/nu,0.5)*pow(pow((j+1)*pow(r,3),(double) 1/3) + pow((i+1)*pow(r,3),(double) 1/3) ,3);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}


// Objet : Noyau d'aggregation pure Levich
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, nu - viscosité cinématique, epsilon - vitesse de dissipation d'énergie massique, taille - nb de classes)
noyauLevich::noyauLevich (double r, double nu, double epsilon, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=13.8*pow(epsilon/nu,0.5)*pow(pow((j+1)*pow(r,3),(double) 1/3) + pow((i+1)*pow(r,3),(double) 1/3) ,3);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}


// Objet : Noyau d'aggregation pure par décantation
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, mu - viscosité dynamique, rho - masse volumique des particules, taille - nb de classes)
noyauDecant::noyauDecant (double r, double mu, double rho, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
			noyauaggr[j+i*(i+1)/2]=2*9.81*3.14156*rho*pow(pow((j+1)*pow(r,3),(double) 1/3) + pow((i+1)*pow(r,3),(double) 1/3) ,2)*abs(pow(pow((j+1)*pow(r,3),(double) 1/3),2)-pow(pow((i+1)*pow(r,3),(double) 1/3),2))/(9*mu);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}


// Objet : Noyau d'efficacité Pnueli (correction simple Saffman-Turner)
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, taille - nb de classes)
effPnueli::effPnueli (double r, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=7.5*pow(pow((j+1)*pow(r,3),(double) 1/3) / pow((i+1)*pow(r,3),(double) 1/3) ,2);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}


// Objet : Noyau d'efficacité Pnueli + effet brownien (correction Saffman-Turner + agitation brownienne)
// Constructeur de noyaux analytiques (r - rayon de particule élémentaire, peclet - nb de Peclet, taille - nb de classes)
effPnueliBrown::effPnueliBrown (double r, double peclet, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;
		
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=4.74*pow(peclet,(double) -2/3)+7.5*pow(pow((j+1)*pow(r,3),(double) 1/3) / pow((i+1)*pow(r,3),(double) 1/3) ,2);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau aléatoire de distribution normale (utilisation principale : perturbation d'un noyau donné par ajout)
// Constructeur de noyaux analytiques (ecarttype - écart-type de la distribution, taille - nb de classes, type - aggregation - 1 ou dislocation - 2 )
noyauNormal::noyauNormal (double ecarttype, int taille, int type)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Préparation de la distribution normale
	std::mt19937 generateur(time(0));
	std::normal_distribution<double> distribution(0.0,ecarttype);
	
	// Aggregation
	if (type == 1)
	{
		// Redimensionne le tableau du noyau aggr
		noyauaggr.resize(taille*(taille+1)/2,0);
		
		// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=distribution(generateur);
			}
		}
			
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
	}
	// Dislocation
	else
	{
		// Redimensionne le tableau du noyau disl
		noyaudisl.resize(taille*(taille+1)/2,0);
		
		// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{	
				noyaudisl[j+i*(i+1)/2]=distribution(generateur);
			}
		}
	}

	// Pas de modèle d'aggrégation
	noyauaggr.resize(taille*(taille+1)/2,0);
	}
}

// Réinitialise le noyau normal, sans modification de taille
// Réinitialisation de noyaux analytiques (ecarttype - écart-type de la distribution, type - aggregation - 1 ou dislocation - 2 )
void noyauNormal::reinitialise (double ecarttype, int type)
{	
	int i,j;

	// Préparation de la distribution normale
	std::mt19937 generateur(time(0));
	std::normal_distribution<double> distribution(0.0,ecarttype);
	
	// Aggregation
	if (type == 1)
	{	
		// Remplissage
		for (i=0;i!=nbclasses;i++)
		{
			for (j=0;j!=i+1;j++)
			{
				if (j+i<nbclasses-1)
				{
					noyauaggr[j+i*(i+1)/2]=distribution(generateur);
				}
			}	
		}

	}
	// Dislocation
	else
	{
		// Remplissage
		for (i=0;i!=nbclasses;i++)
		{
			for (j=0;j!=i+1;j++)
			{
				if (j+i<nbclasses-1)
				{	
					noyaudisl[j+i*(i+1)/2]=distribution(generateur);
				}
			}
		}
	}
}

// Copie un noyau, et ajoute une perturbation aléatoire (distribuée selon loi normale) proportionnelle à ses coefficients 
void noyauNormal::perturbe (double ecarttype, noyau * kernel)
{	
	int i,j;
	nbclasses=kernel->taille();
	
	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(nbclasses*(nbclasses+1)/2,0);
	noyaudisl.resize(nbclasses*(nbclasses+1)/2,0);
		
	// Préparation de la distribution normale
	std::mt19937 generateur(time(0));
	std::normal_distribution<double> distribution(0.0,1.0);
	

	// Remplissage
	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<nbclasses-1)
			{
				noyauaggr[j+i*(i+1)/2]=kernel->element(i+1,j+1,1)*(1+ecarttype*distribution(generateur));
				noyaudisl[j+i*(i+1)/2]=kernel->element(i+1,j+1,2)*(1+ecarttype*distribution(generateur));
			}
		}
	}
}


// Objet : Noyau construit à partir de l'état d'équilibre donné par une distribution
// Constructeur de noyaux expérimentaux (distr - distribution à l'état d'équilibre)
noyauEquilibre::noyauEquilibre (distribution * distr)
{	
	// Préparation nb de classes
	nbclasses=(*distr).taille();
	int i,j;
	// int k;
	// Redimensionne les tableaux du noyau
	noyauaggr.resize(nbclasses*(nbclasses+1)/2,1);
	noyaudisl.resize(nbclasses*(nbclasses+1)/2,0);
	
	/*
	// Remplissage des constantes d'équilibre de base
	for (i=0;i!=nbclasses;i++)
	{
		noyauaggr[i*(i+1)/2]=(*distr).element(i+2)/((*distr).element(i+1)*(*distr).element(1));
	}
	
	// Remplissage des conditions de compatibilité (état d'équilibre global = état d'équilibre local)
	for (i=1;i!=nbclasses;i++) // à modifier !!
	{
		for (j=1;j!=i+1;j++)
		{
			
			// En cas de dépassement de taille du tableau (conditions de compatibilités liées à des coefficients situés bcp plus loin dans le tableau)
			if (i+j<=nbclasses)
			{
				for (k=max(i,j);k!=i+j;k++)
				{
					noyauaggr[j+i*(i+1)/2]=noyauaggr[j+i*(i+1)/2]*noyauaggr[k*(k+1)/2];
				}
				for (k=1;k!=min(i,j);k++)
				{
					noyauaggr[j+i*(i+1)/2]=noyauaggr[j+i*(i+1)/2]/noyauaggr[k*(k+1)/2];
				}
			}
			else
			{
				noyauaggr[j+i*(i+1)/2]=0;
			}
		}
	}
	*/
	
	for (i=0;i!=nbclasses;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<nbclasses-1)
			{	
				noyauaggr[j+i*(i+1)/2]=(*distr).element(j+i+2)/((*distr).element(j+1)*(*distr).element(i+1));
				noyaudisl[j+i*(i+1)/2]=1;
			}
			else
			{
				noyauaggr[j+i*(i+1)/2]=0;
			}
		}
	}
	
	
}

// Objet : Noyau d'aggregation pure de type produit
// Constructeur de noyaux analytiques (k coefficient multiplicateur, taille - nb de classes, modèle utilisé : purement mathématique, produit)
noyauAggProd::noyauAggProd (double k, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=k*(i+1)*(j+1);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau d'aggregation pure de type somme
// Constructeur de noyaux analytiques (k coefficient multiplicateur, taille - nb de classes, modèle utilisé : purement mathématique, somme)
noyauAggSom::noyauAggSom (double k, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=k*(i+j+2);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau de fragmentation pure de type loi puissance
// Constructeur de noyaux analytiques (k coefficient multiplicateur, n - exposant, taille - nb de classes, modèle utilisé : purement mathématique, loi puissance)
noyauDislPuis::noyauDislPuis (double k, double n, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyaudisl.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyaudisl[j+i*(i+1)/2]=k * pow( (i+j+2) , (double) n);
			}
		}
	}

	// Pas de modèle de dislocation
	noyauaggr.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau de fragmentation pure modèle de type loi exponentielle (première variante)
// Constructeur de noyaux analytiques (k coefficient multiplicateur, l coefficient multiplicateur exponentielle, n - exposant, taille - nb de classes, modèle utilisé : physique-forme généralisée, loi exponentielle simple)
noyauDislModeleExp1::noyauDislModeleExp1 (double k, double l, double n, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyaudisl.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyaudisl[j+i*(i+1)/2]=k * exp( -l * pow( (double) 1/(i+j+2) , (double) n));
			}
		}
	}

	// Pas de modèle d'agrégation
	noyauaggr.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau de fragmentation pure modèle de type loi exponentielle (seconde variante)
// Constructeur de noyaux analytiques (k coefficient multiplicateur, l coefficient multiplicateur exponentielle, n - exposant, taille - nb de classes, modèle utilisé : physique-forme généralisée, loi exponentielle complexe)
noyauDislModeleExp2::noyauDislModeleExp2 (double k, double l, double n, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyaudisl.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyaudisl[j+i*(i+1)/2]=k * exp( -l*pow((double) 1/(i+j+2) , (double) n)) * pow((i+j+2), (double) -2/9);
			}
		}
	}

	// Pas de modèle d'agrégation
	noyauaggr.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau d'aggregation pure modèle de type brownien
// Constructeur de noyaux analytiques (k coefficient multiplicateur, taille - nb de classes, modèle utilisé : physique-forme généralisée, brownien)
noyauAggModeleBrown::noyauAggModeleBrown (double k, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=k/(pow(i+1, (double) 1/3) * pow(j+1, (double) 1/3))* pow( (pow(i+1, (double) 1/3) + pow(j+1, (double) 1/3)), (float) 2);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau d'aggregation pure modèle de type turbulent (première variante)
// Constructeur de noyaux analytiques (k coefficient multiplicateur, taille - nb de classes, modèle utilisé : physique-forme généralisée, brownien)
noyauAggModeleTurb1::noyauAggModeleTurb1 (double k, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=k* pow( (pow(i+1, (double) 1/3) + pow(j+1, (double) 1/3)), (float) 2);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}

// Objet : Noyau d'aggregation pure modèle de type turbulent (seconde variante)
// Constructeur de noyaux analytiques (k coefficient multiplicateur, taille - nb de classes, modèle utilisé : physique-forme généralisée, brownien)
noyauAggModeleTurb2::noyauAggModeleTurb2 (double k, int taille)
{	
	// Préparation nb de classes
	nbclasses=taille;
	int i,j;

	// Redimensionne le tableau du noyau aggr
	noyauaggr.resize(taille*(taille+1)/2,0);

	// Remplissage
	for (i=0;i!=taille;i++)
	{
		for (j=0;j!=i+1;j++)
		{
			if (j+i<taille-1)
			{
				noyauaggr[j+i*(i+1)/2]=k* pow( (pow(i+1, (double) 1/3) + pow(j+1, (double) 1/3)), (float) 3);
			}
		}
	}

	// Pas de modèle de dislocation
	noyaudisl.resize(taille*(taille+1)/2,0);
}