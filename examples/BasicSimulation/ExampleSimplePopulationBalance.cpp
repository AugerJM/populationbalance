//Population balance example : simple case

#include <stdio.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include "Noyau.h"
#include "Distribution.h"

using namespace std;



int main(void)
{
	int i;
	double paseff;
	clock_t t1,t2;

	cout << endl << "Population balance example : simple case" << endl << endl;	
	
	// Starting distribution
	distribution dis("EssaiDistributionParticules.csv");
	
	// Kernel construction (constant aggregation and breakage rates)
	noyauConst kernel(1,0.01,10);

	// Kernel on screen writing	
	kernel.ecrire();
	getchar();

	// Computation time recording
    	t1=clock();
	
	// Increasing time
	for (i=0;i!=40000;i++)
	{
		// Time step proper
		paseff = dis.incremente(&kernel,0.0015);
		// Step recorded in file
		dis.ecrire("Evolution.txt");
	}
	
	// Final distribution state writing
	dis.ecrire();
	
	// End of computation time + writing on screen
	t2=clock();
    	float diff ((float)t2-(float)t1);
    	cout << "Computation time = " << diff/(CLOCKS_PER_SEC) << "s" << endl;
 
    	return 0;
 }