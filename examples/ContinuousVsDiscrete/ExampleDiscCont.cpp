//Population balance example : continuous / discrete comparison

#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include "Noyau.h"
#include "DistributionQuant.h"
#include "Distribution.h"
using namespace std;



int main(void)
{
	// Control variables for loops
	int i;

	// Total number of unitary particles
	float n=48000;

	// Volume considered
	double volume=160000;

	// Timer
	clock_t t1,t2;

	// Files manipulation variables
	ofstream fichier;
	char nom[50];

	// Effective simulated duration for quantized case
	double dureeef;

	// Kernels
	noyau * kernelbase;
	noyau * NoyauTh2;
	

	cout << endl << "Population balance example : continuous / discrete comparison " << endl << endl;


	// Computation starting time
    	t1=clock();


	// Initial state generation : the starting distribution contains only unitary particles

	// Discrete case - initial state stored in file
	sprintf(nom, "Initial.txt");
	fichier.open(nom, ios::out);
	fichier << volume << endl;
	fichier << std::fixed << n << endl;
	fichier.close();

	// Continuous case - initial state stored in file
	sprintf(nom, "InitialCont.txt");
	fichier.open(nom, ios::out);
	fichier << n/volume << endl;
	fichier.close();

	// Initialization for quantized case - initial state displayed
	distributionquant simulation("Initial.txt");
	simulation.ecrire();

	// Initialization for continuous case - initial state displayed	
	distribution simulationcont("InitialCont.txt");
	simulationcont.ecrire();
	

	// Kernel creation

	// Aggregation part
	kernelbase = new noyauAggModeleTurb2 (4, 1000);

	// Breakage part
	NoyauTh2 = new noyauDislModeleExp1 (0.5, 2, 0.25, 1000);

	// Merging
	kernelbase->ajout(NoyauTh2);
	delete NoyauTh2;

	
	// Simulations

	// Population balance simulation - continuous case (10s)
	i=0;
	while(i<10000)
	{
		i++;
		simulationcont.incremente(kernelbase,0.001);
	}
	simulationcont.ecrire("resultatcont.txt");

	// Population balance simulation - discrete case (10s at least)
	dureeef += simulation.incremente(kernelbase,10.0);
	// Displaying the real simulated duration (time of next event following the programmed 10s duration)
	cout << "Effectively simulated duration (discrete) = " << dureeef << "s" << endl;
	simulation.ecrire("resultatdisc.txt");


	// Computation time calculations
	t2=clock();
    	float diff ((float)t2-(float)t1);
   	cout << "DUration : " << diff/(CLOCKS_PER_SEC) << "s" << endl;
    	return 0;
}