//Population balance example : comparison between multiple starting points

#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include "Noyau.h"
#include "Distribution.h"

using namespace std;



int main(void)
{
	// Variables for loops
	int i,j,k;

	// Timer
	clock_t t1,t2;

	// Maximal time step
	double pas=0.0015;

	// Stopping condition for time increments (Cauchy type criterion limit)
	double distance=0.000000000000000001;

	// Flux and names for recording files
	ofstream fichier;
	char nom[50];
	char nom2[50];

	// Kernel size
	int N=10;

	// Elementary particles total concentration
	float n;

	// Distribution lists (correspond to different initial states)
	vector<distribution> liste;
	vector<distribution> liste2;

	// Simulated durations
	vector<double> tps (N);
	vector<double> tps2 (N);

	cout << endl << "Population balance example : comparison between multiple starting points for multiple unitary particles concentrations" << endl << endl;
	
	// Kernel construction (sum model for aggreggation, constant for breakage part)
	noyauAggSom kernel(0.0250,N);
	noyauConst kernel2(0,1,N);
	kernel.ajout(&kernel2);

	// Kernel is written on screen for control
	kernel.ecrire();

	// Starting computation timer
    	t1=clock();

	// Loop on unitary particles concentrations (1 to 1000 by 50)
	for(k=1;k<1000;k=k+50)
	{
		// Filename for the current concentration
		sprintf(nom2, "Final%04d.txt", k);
		n=k;

		// Creation of initial states (all unitary particles are present as one cluster size only)
		for (i = N; i!=0 ; i--)
		{
			// Filenames for initial states
	   		sprintf(nom, "Initial%03d.txt", i);
			fichier.open(nom, ios::out);

			// Files filling
			for (j=1; j!=N+1; j++)
			{
				if (j==i)
				{
					fichier << n/i << endl;
				}
				else
				{
					fichier << "0" << endl;
				}
			}
	   		fichier.close();
		}

		// Creation of distributions from initial states files (each distribution is created once per list)
		for ( i = 1; i!=N+1 ; i++)
		{
	   		sprintf(nom, "Initial%03d.txt", i);
			liste.push_back(distribution(nom));
			liste2.push_back(distribution(nom));
			tps[i-1]=0;
			tps2[i-1]=0;
		}

		// First time step applied to first list only (ensures that distributions corresponding to a same initial state in the 2 lists differ by one time step advance)
		for ( i = 1; i!=N+1 ; i++)
		{
			tps[i-1]+=liste[i-1].incremente(&kernel,pas);
		}

		// For each distribution in the lists
		for ( i = 1; i!=N+1 ; i++)
		{
			// While a Cauchy criterion (distance between distributions corresponding to consecutive time steps) is unmet, the distribution is considered far from equilibrium
			while (liste[i-1].distance (&liste2[i-1], "Bhattacharyya")/(tps[i-1]-tps2[i-1])*pas > distance)
			{
				tps[i-1]+=liste[i-1].incremente(&kernel,pas);
				tps2[i-1]+=liste2[i-1].incremente(&kernel,pas);
			}
		}

		// Computation and recording of distances between steady states corresponding to a given unitary particles concentration
		fichier.open(nom2, ios::out);
		for ( i = 1; i!=N+1 ; i++)
		{
			for ( j = 1; j!=N+1 ; j++)
			{
				fichier << liste[i-1].distance (&liste[j-1], "Bhattacharyya") << " ";
			}
			fichier << endl;
		}
		fichier << endl;
		fichier.close();

		// Recording of steady states
		for ( i = 1; i!=N+1 ; i++)
		{
			liste[i-1].ecrire(nom2,tps[i-1]);
		}
	}
	
	// End of computation timer
	t2=clock();
    	float diff ((float)t2-(float)t1);
   	cout << "Simulation duration = " << diff/CLOCKS_PER_SEC << "s" << endl;

    	return 0;
 }