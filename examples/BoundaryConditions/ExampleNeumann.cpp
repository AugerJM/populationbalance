//Population balance example : Neumann boundaries conditions

#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include "Noyau.h"
#include "Distribution.h"

using namespace std;



int main(void)
{
	int i;
	double paseff;

	cout << endl << "Population balance example : Neumann boundaries conditions" << endl << endl;	
	
	// Starting distribution created from file
	distribution dis("EssaiDistributionParticules.csv");
	
	// Kernel created from files (no complexes balance possible)
	noyau kernel("Noy1.txt","Noy2.txt");

	// Population balance computation (time steps)
	for (i=0;i!=200000;i++)
	{
		// Time step proper
		paseff = dis.incremente(&kernel,0.0015);

		// Neumann conditions application, writing the corresponding fluxes on screen
		cout << dis.neumann(1,0.1) << endl;
		cout << dis.neumann(20,0.01) << endl;

		// Distribution recording in file 
		dis.ecrire("Concentrations.txt");
	}
	
    	return 0;
 }