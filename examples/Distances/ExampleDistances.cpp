//Population balance example : comparison between 2 similar kernels through distance measurements

#include <stdio.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include "Noyau.h"
#include "Distribution.h"

using namespace std;



int main(void)
{
	int i;
	double paseff;

	cout << endl << "Population balance example : comparison between 2 similar kernels through distance measurements" << endl << endl;	
	
	// Starting point distribution from file
	distribution dis("EssaiDistributionParticules.csv");
	distribution dis2("EssaiDistributionParticules.csv");
	
	// Creation of 2 kernels (in this case, kernels are constants but differ in size/biggest allowed cluster)
	noyauConst kernel1(1,0.01,200);
	noyauConst kernel2(1,0.01,20);

	// File recording
	ofstream fichier;
	fichier.open("Distances.txt", ios::out);
	
	// Population balance computation
	for (i=0;i!=50000;i++)
	{
		// Each distribution/kernel couple is applied a time step
		paseff = dis.incremente(&kernel1,0.0015);
		paseff = dis2.incremente(&kernel2,0.0015);
		
		// Recording of distributions distance for each time step
		fichier << dis.distance(&dis2,"Bhattacharyya") << endl;
	}

	// File closing
	fichier.close();

    	return 0;
 }